//
//  TruckTypeListViewController.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-06.
//

import UIKit

class TruckTypeListViewController: UIViewController, addTruckTypeProtocol, UISearchBarDelegate {

    func refresh() {
        makeGetCall()
    }
    @IBOutlet var searchBarView: UISearchBar!
    
    @IBOutlet weak var searchBUtton: UIBarButtonItem!
    var searchMode = false
    var searchResult =  [TruckTypeList]()
    @IBOutlet weak var truckTypeTableView: UITableView!
    var truckTypeList : TruckTypeJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Truck Type"
        truckTypeTableView.delegate = self
        truckTypeTableView.dataSource = self
       
        makeGetCall()
        searchBarView.delegate = self

    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        search(searchTExt: searchText)
        
    }
    func search(searchTExt : String)
    {
        searchResult.removeAll()
        print(searchTExt)
        if truckTypeList != nil
        {
            if let truck = truckTypeList?.truckTypeList
            {
                for x in truck
                {
                    if x.truckTypeName?.containsIgnoringCase(find: searchTExt) == true
                    {
                        searchResult.append(x)
                    }
                }
            }
        }
        truckTypeTableView.reloadData()
        
    }

    @IBAction func searchButtonPressed(_ sender: Any) {
        
        if searchMode
        {
            searchBUtton.image = UIImage(systemName: "magnifyingglass")
            searchMode = false
            let textFieldInsideSearchBar = searchBarView.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.text = ""
            self.navigationItem.titleView = nil
            self.truckTypeTableView.reloadData()
        }
        else
        {
            searchMode  = true
            searchBUtton.image = UIImage(systemName: "xmark")
        searchBarView.sizeToFit()
            let textFieldInsideSearchBar = searchBarView.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.becomeFirstResponder()

            textFieldInsideSearchBar?.textColor = .white
        self.navigationItem.titleView = searchBarView
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addTruckType"
        {
            let vc = segue.destination as! AddTruckType
            vc.delegate = self
        }
        if segue.identifier == "updateTruckType"
        {
            let vc = segue.destination as! AddTruckType
            vc.delegate = self
            let x = sender as! TruckTypeList
            vc.editData = x
        }
    }
    
}


extension TruckTypeListViewController: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if searchMode
        {
             let x = searchResult[indexPath.row]
            
            cell.textLabel?.text = x.truckTypeName?.uppercased() ?? ""
            cell.textLabel?.textColor = constantsUsedInApp.accentColor
            
        }
        else
        {
        if let x = truckTypeList?.truckTypeList?[indexPath.row]
        {
          //  cell.clientAddress.text = x.clientDetails?.uppercased() ?? ""
            //cell.clientName.text = x.clientName?.uppercased() ?? ""
            cell.textLabel?.text = x.truckTypeName?.uppercased() ?? ""
            cell.textLabel?.textColor = constantsUsedInApp.accentColor
        }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchMode
        {
            return searchResult.count
        }
        else
        {
        return truckTypeList?.truckTypeList?.count ?? 0
        }
        
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        //  action.backgroundColor = .yellow
        let actionApply = UIContextualAction(style: .normal, title:  "Delete", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            success(true)
            let alert = UIAlertController(title: "Delete", message: "Are You Sure?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
                
                let x = truckTypeList?.truckTypeList?[indexPath.row]
                let json : [String : Any] = ["truck_type_id":"\(x?.truckTypeID ?? "")","truck_type_name":"\(x?.truckTypeName ?? "")","code":0,"created_by":"\(x?.createdBy ?? "")","created_date":"\(x?.createdDate ?? "")"]
                makePostDelete(json: json, url: "\(constantsUsedInApp.baseUrl)taform/deleteTrucktype", index: indexPath)

            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            
        })
        
        let actionEdit = UIContextualAction(style: .normal, title:  "Edit", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            success(true)
              self.truckTypeTableView.reloadData()
            self.performSegue(withIdentifier: "updateTruckType", sender: truckTypeList?.truckTypeList?[indexPath.row])
            
        })
        
        actionApply.backgroundColor = .systemRed
        //  actionApply.image = UIImage(systemName: "hand.tap.fill")
        
        return UISwipeActionsConfiguration(actions: [actionApply , actionEdit])
    }
    
    func makeGetCall() {
        let decoder = JSONDecoder()
        
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllTrucktypes")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TruckTypeJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            self.truckTypeList = nil
                            print(loginBaseResponse as Any)
                            self.truckTypeList = loginBaseResponse
                            self.truckTypeTableView.reloadData()
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Location", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    func makePostDelete(json: [String: Any] , url : String , index : IndexPath) {
        let decoder = JSONDecoder()
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(url)")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TaSaveJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.makeGetCall()
                            let alert = UIAlertController(title: "Advance", message: "\(loginBaseResponse?.response ?? "Delete")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                print("delete")
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Advance", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}

//
//  ExpenseDetails.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-06.
//

import UIKit

class ExpenseDetails: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var notes: UITextField!
    @IBOutlet weak var card: UITextField!
    @IBOutlet weak var cash: UITextField!
    var advanceTypes : AdvanceTypeJSON?

    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var expenseType: UITextField!
    @IBOutlet weak var expenseTableView: UITableView!
    var textfieldPickers = [UITextField]()
    var expenseArray = [ExpenseDataType]()
    var deletedsExpenseIDs = [String] ()

    override func viewDidLoad() {
        super.viewDidLoad()
        cash.delegate = self
        card.delegate = self
        notes.delegate = self
        cash.tag = 99
        card.tag = 98
        notes.tag = 97
        dismissPickerViewForTextField(textField: cash)
        dismissPickerViewForTextField(textField: card)
     makeGetCall()
        saveButton.layer.cornerRadius = 10

        expenseTableView.delegate = self
        expenseTableView.dataSource = self
        expenseTableView.register(UINib(nibName: reuseNibIdentifier.expenseCell, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.expenseCell)
        textfieldPickers = [expenseType]
        for textField in 0 ... textfieldPickers.count - 1
        {
            textfieldPickers[textField].delegate = self
            createPickerView(textField: textfieldPickers[textField], tag: textField)
            //textfieldPickers[textField].font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        dismissPickerView()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == cash || textField == card
        {
            let y = Double(textField.text ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            textField.text = s
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == card || textField == cash
        {
            if textField.text == "0.00"
            {
                textField.text = ""
            }
        }

        return true
    }

    func isEntriesEmpty() -> Bool
    {
        if expenseArray.count == 0
        {
            return true

        }
        else
        {
            return false
        }
    }
    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in textfieldPickers
        {
            textField.inputAccessoryView = toolBar
        }
        
    }
    func dismissPickerViewForTextField(textField : UITextField) {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
            textField.inputAccessoryView = toolBar
        
        
    }

    @objc func action() {
        view.endEditing(true)
    }

    func makeGetCall() {
        let decoder = JSONDecoder()
      
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAdvanceList")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(AdvanceTypeJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.advanceTypes = loginBaseResponse
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    @IBAction func saveButtonPressed(_ sender: Any) {
        print("save")
        if expenseType.text == "" || card.text == "" || cash.text == ""
        {
            let alert = UIAlertController(title: "New Booking", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let x = ExpenseDataType(expenseType: expenseType.text ?? "", cash: cash.text ?? "", card: card.text ?? "", notes: notes.text ?? "")
            expenseArray.append(x)
          
            expenseTableView.reloadData()
            expenseType.text = ""
            cash.text = "0.00"
            card.text = "0.00"
            notes.text = ""
        }
    }
   
}



extension ExpenseDetails: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = expenseTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.expenseCell) as! ExpenseTableCell
        let exp = expenseArray[indexPath.row]
        let y = Double(exp.card )
        let s = String(format: "%.2f", y ?? 0.0)

        cell.cardLabel.text = s
        let z = Double(exp.cash )
        let d = String(format: "%.2f", z ?? 0.0)

        cell.cashLabel.text = d
        cell.notesLabel.text = exp.notes
        cell.expenseTypeLabel.text = exp.expenseType
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return expenseArray.count
        
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
         {
        
            //  action.backgroundColor = .yellow
            let actionApply = UIContextualAction(style: .normal, title:  "Delete", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                      print("Apply")
                  success(true)
                if expenseArray[indexPath.row].id != nil
                {
                    deletedsExpenseIDs.append(expenseArray[indexPath.row].id!)
                }
                expenseArray.remove(at: indexPath.row)
                expenseTableView.reloadData()
                                  
                
                  })
       
          actionApply.backgroundColor = .systemRed
        //  actionApply.image = UIImage(systemName: "hand.tap.fill")

                  return UISwipeActionsConfiguration(actions: [actionApply])
            }


    
    
}


extension ExpenseDetails : UIPickerViewDelegate , UIPickerViewDataSource
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textfieldPickers[0].text = advanceTypes?.advanceList?[0].advanceName ?? ""

        default:
            print("err")
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return advanceTypes?.advanceList?.count ?? 0
        case 1:
            return 3
        case 2:
            return 2
        case 3:
            return 3
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return advanceTypes?.advanceList?[row].advanceName ?? ""
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            textfieldPickers[0].text = advanceTypes?.advanceList?[row].advanceName ?? ""
        
        default:
            print("err")
            print(pickerView.tag)
        }
    }
}








struct ExpenseDataType {
    var expenseType : String
    var cash : String
    var card : String
    var notes : String
    var id : String?
    var createBy : String?
    var createDate : String?
    var modifyby : String?
    var loginiD : String?
    var modifyDate : String?
    var taformID : String?
    var expenseTypeID : String?
}

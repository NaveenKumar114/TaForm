//
//  JobDetails.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-03.
//

import UIKit
import DatePickerDialog

class JobDetails: UIViewController, UITextFieldDelegate, ngihtStayProtocol {
    func closing(data: nightStayData) {
        nightStayData = data
        nightStayLabel.text =  "\(nightStayData?.noOfDAys ?? "") days"
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
     
        return false

    }

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == comissionTextField
        {
            let y = Double(comissionTextField.text ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            comissionTextField.text = s
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == comissionTextField
        {
            if textField.text == "0.00"
            {
                textField.text = ""
            }
        }

        return true
    }
    func isEntriesEmpty() -> Bool
    {
        if multiJobArray.count == 0
        {
            return true

        }
        else
        {
            return false
        }
    }

    @IBOutlet weak var clientNameTextField: UITextField!
    @IBOutlet weak var jobDateTextField: UILabel!
    
    @IBOutlet weak var multiJobTableView: UITableView!
    
    @IBOutlet weak var nightStayLabel: UILabel!
    @IBOutlet weak var comissionTextField: UITextField!
    
    @IBOutlet weak var remarksTextField: UITextField!
    @IBOutlet weak var jobNumberTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var fromTextField: UITextField!
    var textfieldPickers = [UITextField]()
    @IBOutlet weak var truckTypeTextField: UITextField!
    @IBOutlet weak var truckNUmberTextField: UITextField!
    var clientList : ClientListJSON?
    var truckNumebrList : TruckNumberJSON?
    var truckTypeList : TruckTypeJSON?
    var locationList : LocationListJSON?
    var nightStayData : nightStayData?
    var selectedClient : ClientsList?
    var selectedFrom : LocationsList?
    var selectedTo : LocationsList?
    var selectedTruckNUmber : TruckNumberList?
    var selectedTRuckType : TruckTypeList?
    var multiJobArray = [jobDetailsDataType] ()
    var deletedjobIDs = [String] ()

    override func viewDidLoad() {
        jobNumberTextField.delegate = self
        jobNumberTextField.tag = 99
        comissionTextField.delegate = self
        comissionTextField.tag = 98
        remarksTextField.delegate = self
        remarksTextField.tag = 97
        dismissPickerViewForTextField(textField: comissionTextField)
        
        makeGetCall()
        makeGetCallTruckType()
        makeGetLocationList()
        makeGetTruckNumberList()
        textfieldPickers = [clientNameTextField , truckTypeTextField , truckNUmberTextField , fromTextField , toTextField]
        jobDateTextField.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        jobDateTextField.addGestureRecognizer(gesture)
        for textField in 0 ... textfieldPickers.count - 1
        {
            textfieldPickers[textField].delegate = self
            createPickerView(textField: textfieldPickers[textField], tag: textField)
           // textfieldPickers[textField].font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        multiJobTableView.delegate = self
        multiJobTableView.dataSource = self
        multiJobTableView.register(UINib(nibName: reuseNibIdentifier.multiJOb, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.multiJOb)
        dismissPickerView()
        nightStayLabel.isUserInteractionEnabled = true
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(toNightLabel))
        nightStayLabel.addGestureRecognizer(gesture2)
    }
    @objc func toNightLabel()
    {
        performSegue(withIdentifier: "nightStay", sender: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "nightStay"
        {
            let x = segue.destination as! NightStayAdvance
            x.delegate = self
    
        }
    }
    
    @IBAction func multiJobSaveButtonPressed(_ sender: Any) {
        if clientNameTextField.text == "" || jobDateTextField.text == "Select Date" || comissionTextField.text == "" ||  jobNumberTextField.text == "" || toTextField.text == "TO" || fromTextField.text == "FROM" || truckTypeTextField.text == "TRUCK TYPE" || truckNUmberTextField.text == "TRUCK NUMBER"
        {
            let alert = UIAlertController(title: "Job Details", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)

        }
        else
        {
            let x = jobDetailsDataType(nightStay: nightStayData, date: jobDateTextField.text!, jNo: jobNumberTextField.text!, client: selectedClient, from: selectedFrom, to: selectedTo, truckType: selectedTRuckType, truckNumber: selectedTruckNUmber, comission: comissionTextField.text!, remark: remarksTextField.text ?? "")
            multiJobArray.append(x)
            multiJobTableView.reloadData()
            clientNameTextField.text = ""
            jobDateTextField.text = "Select Date"
            comissionTextField.text = ""
            remarksTextField.text = ""
            jobNumberTextField.text = ""
            toTextField.text = "TO"
            fromTextField.text = "FROM"
            truckTypeTextField.text = "TRUCK TYPE"
            truckNUmberTextField.text = "TRUCK NUMBER"
        }

    }
    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in textfieldPickers
        {
            textField.inputAccessoryView = toolBar
        }
        
    }
    
    func dismissPickerViewForTextField(textField : UITextField) {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
            textField.inputAccessoryView = toolBar
        
        
    }
    @objc func action() {
        view.endEditing(true)
    }
    @objc func showDateOrTime(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UILabel
        switch x {
        case jobDateTextField :
        datePickerTapped(sender: x!)
            print("date")

        default:
            print("error in time date")
        }
        
    }
    func datePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                sender.text = formatter.string(from: dt)
                // print(formatter.string(from: dt))
            }
        }
        
    }
    func makeGetCall() {
        let decoder = JSONDecoder()
      
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllClients")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(ClientListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.clientList = loginBaseResponse
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func makeGetCallTruckType() {
        let decoder = JSONDecoder()
      
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllTrucktypes")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TruckTypeJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.truckTypeList = loginBaseResponse
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func makeGetTruckNumberList() {
        let decoder = JSONDecoder()
      
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllTruckNumber")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TruckNumberJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.truckNumebrList = loginBaseResponse
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func makeGetLocationList() {
        let decoder = JSONDecoder()
      
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllLocations")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(LocationListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.locationList = loginBaseResponse
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}

extension JobDetails : UIPickerViewDelegate , UIPickerViewDataSource
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == ""
        {
        switch textField.tag {
        case 0:
            textfieldPickers[0].text = clientList?.clientsList?[0].clientName ?? ""

        default:
            print("err")
        }
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return clientList?.clientsList?.count ?? 0
        case 1:
            return truckTypeList?.truckTypeList?.count ?? 0
        case 2:
            return truckNumebrList?.truckNumberList?.count ?? 0
        case 3:
            return locationList?.locationsList?.count ?? 0
        case 4:
            return locationList?.locationsList?.count ?? 0
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return clientList?.clientsList?[row].clientName ?? ""
        case 1:
            return truckTypeList?.truckTypeList?[row].truckTypeName ?? ""
        case 2 :
            return truckNumebrList?.truckNumberList?[row].truckNo ?? ""
        case 3 :
            return locationList?.locationsList?[row].location ?? ""
        case 4 :
            return locationList?.locationsList?[row].location ?? ""
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            textfieldPickers[0].text = (clientList?.clientsList?[row].clientName ?? "").uppercased()
            selectedClient = clientList?.clientsList?[row]
        case 1:
            textfieldPickers[1].text =  (truckTypeList?.truckTypeList?[row].truckTypeName ?? "").uppercased()
            selectedTRuckType = truckTypeList?.truckTypeList?[row]
        case 2 :
            textfieldPickers[2].text = (truckNumebrList?.truckNumberList?[row].truckNo ?? "").uppercased()
            selectedTruckNUmber = truckNumebrList?.truckNumberList?[row]
        case 3 :
            textfieldPickers[3].text = (locationList?.locationsList?[row].location ?? "").uppercased()
            selectedFrom = locationList?.locationsList?[row]
        case 4 :
            textfieldPickers[4].text = (locationList?.locationsList?[row].location ?? "").uppercased()
            selectedTo = locationList?.locationsList?[row]
        default:
            print("err")
            print(pickerView.tag)
        }
    }
}






extension JobDetails: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = multiJobTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.multiJOb) as! MultiJobCell
        let x = multiJobArray[indexPath.row]
        cell.date.text = x.date
        cell.to.text = x.to?.location?.uppercased()
        cell.from.text = x.from?.location?.uppercased()
        cell.type.text = x.truckType?.truckTypeName
        cell.number.text = x.truckNumber?.truckNo
        cell.jobNo.text = x.jNo
        let y = Double(x.comission ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)

        cell.comission.text = "\(s) MYR"
        print(x)
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return multiJobArray.count
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
         {
        
            //  action.backgroundColor = .yellow
            let actionApply = UIContextualAction(style: .normal, title:  "Delete", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                      print("Apply")
                  success(true)
                if multiJobArray[indexPath.row].id != nil
                {
                    deletedjobIDs.append(multiJobArray[indexPath.row].id!)
                }
                multiJobArray.remove(at: indexPath.row)
                multiJobTableView.reloadData()
                                  
                  })
       
          actionApply.backgroundColor = .systemRed
        //  actionApply.image = UIImage(systemName: "hand.tap.fill")

                  return UISwipeActionsConfiguration(actions: [actionApply])
            }


    
    
}

struct jobDetailsDataType {
    var nightStay : nightStayData?
    var date : String?
    var jNo : String?
    var client : ClientsList?
    var from : LocationsList?
    var to : LocationsList?
    var truckType : TruckTypeList?
    var truckNumber : TruckNumberList?
    var comission : String?
    var remark : String?
    var id : String? // jobid
    var createBy : String?
    var createDate : String?
    var taformID : String?
}

//
//  TruckNumberViewController.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-07.
//

import UIKit

class TruckNumberViewController: UIViewController, addTruckNUmberProtocol, UISearchBarDelegate {
    func refresh() {
        makeGetCall()
    }
    @IBOutlet var searchBarView: UISearchBar!
    
    @IBOutlet weak var searchBUtton: UIBarButtonItem!
    var searchMode = false
    var searchResult =  [TruckNumberList]()
    
    @IBOutlet weak var truckNumberTableView: UITableView!
    var truckNumberList : TruckNumberJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Truck Number"
        truckNumberTableView.delegate = self
        truckNumberTableView.dataSource = self
        truckNumberTableView.register(UINib(nibName: reuseNibIdentifier.clientList, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.clientList)
        makeGetCall()
        searchBarView.delegate = self

    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        search(searchTExt: searchText)
        
    }
    func search(searchTExt : String)
    {
        searchResult.removeAll()
        print(searchTExt)
        if truckNumberList != nil
        {
            if let truck = truckNumberList?.truckNumberList
            {
                for x in truck
                {
                    if x.truckNo?.containsIgnoringCase(find: searchTExt) == true
                    {
                        searchResult.append(x)
                    }
                }
            }
        }
        truckNumberTableView.reloadData()
        
    }

    @IBAction func searchButtonPressed(_ sender: Any) {
        
        if searchMode
        {
            searchBUtton.image = UIImage(systemName: "magnifyingglass")
            searchMode = false
            let textFieldInsideSearchBar = searchBarView.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.text = ""
            self.navigationItem.titleView = nil
            self.truckNumberTableView.reloadData()
        }
        else
        {
            
            searchMode  = true
            searchBUtton.image = UIImage(systemName: "xmark")
        searchBarView.sizeToFit()
            let textFieldInsideSearchBar = searchBarView.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.becomeFirstResponder()
            textFieldInsideSearchBar?.textColor = .white
        self.navigationItem.titleView = searchBarView
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addtruck"
        {
            let vc = segue.destination as! AddTruckNUmber
            vc.delegate = self
        }
        if segue.identifier == "updatetruck"
        {
            let vc = segue.destination as! AddTruckNUmber
           vc.delegate = self
            let x = sender as! TruckNumberList
            vc.editData = x
        }
    }
    
    
}


extension TruckNumberViewController: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = truckNumberTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.clientList) as! ClientListCell
        if searchMode
        {
             let x = searchResult[indexPath.row]
            
            cell.clientAddress.text = x.truckType?.uppercased() ?? ""
            cell.clientName.text = x.truckNo?.uppercased() ?? ""
            
        }
        else
        {
        if let x = truckNumberList?.truckNumberList?[indexPath.row]
        {
            cell.clientAddress.text = x.truckType?.uppercased() ?? ""
            cell.clientName.text = x.truckNo?.uppercased() ?? ""
        }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchMode
        {
            return searchResult.count
        }
        else
        {
        return truckNumberList?.truckNumberList?.count ?? 0
        }
        
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        //  action.backgroundColor = .yellow
        let actionApply = UIContextualAction(style: .normal, title:  "Delete", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            success(true)
            let alert = UIAlertController(title: "Delete", message: "Are You Sure?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
                
                let x = truckNumberList?.truckNumberList?[indexPath.row]
                let json : [String : Any] = ["truck_no":"\(x?.truckNo ?? "")","truck_number_id":"\(x?.truckNumberID ?? "")","truck_type":"\(x?.truckType ?? "")","code":0,"create_by":"\(x?.createBy ?? "")","create_date":"\(x?.createDate ?? "")","modify_by":"\(x?.modifyBy ?? "")","modify_date":"\(x?.modifyDate ?? x?.createDate ?? "")"]
                makePostDelete(json: json, url: "\(constantsUsedInApp.baseUrl)taform/deleteTrucknumber", index: indexPath)

            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            
        })
        
        let actionEdit = UIContextualAction(style: .normal, title:  "Edit", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            success(true)
              self.truckNumberTableView.reloadData()
            self.performSegue(withIdentifier: "updatetruck", sender: truckNumberList?.truckNumberList?[indexPath.row])
            
        })
        
        actionApply.backgroundColor = .systemRed
        //  actionApply.image = UIImage(systemName: "hand.tap.fill")
        
        return UISwipeActionsConfiguration(actions: [actionApply , actionEdit])
    }
    
    func makeGetCall() {
        let decoder = JSONDecoder()
        
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllTruckNumber")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TruckNumberJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            self.truckNumberList = nil
                            print(loginBaseResponse as Any)
                            self.truckNumberList = loginBaseResponse
                            self.truckNumberTableView.reloadData()
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    func makePostDelete(json: [String: Any] , url : String , index : IndexPath) {
        let decoder = JSONDecoder()
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(url)")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TaSaveJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.makeGetCall()
                            let alert = UIAlertController(title: "Client", message: "\(loginBaseResponse?.response ?? "Delete")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                print("delete")
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Client", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}

//
//  DriverList.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-13.
//

import UIKit

class DriverList: UIViewController {

    @IBOutlet weak var driverTableView: UITableView!
    var driverList : DriverListJSON?
    var selectedDriver : DriversList?
    override func viewDidLoad() {
        super.viewDidLoad()
        driverTableView.delegate = self
        driverTableView.dataSource = self
        driverTableView.register(UINib(nibName: reuseNibIdentifier.driverList, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.driverList)
        makeGetCall()
        self.title = "Driver List"
        let bu = UIBarButtonItem(title: "Add Driver", style: .plain, target: self, action: #selector(self.action))
        self.navigationItem.rightBarButtonItem  = bu
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.reloadList),
            name: Notification.Name("reloadlist"),
            object: nil)
    }
    @objc func reloadList()
    {
        makeGetCall()
    }
    @objc func action()
    {
        performSegue(withIdentifier: "newDriver", sender: nil)
    }
    func makeGetCall() {
        let decoder = JSONDecoder()
      
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllDrivers")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(DriverListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            self.driverList = nil
                            print(loginBaseResponse as Any)
                            self.driverList = loginBaseResponse
                            self.driverTableView.reloadData()
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

   

}

extension DriverList: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = driverTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.driverList) as! DriverListCell
       if  let x = driverList?.driversList?[indexPath.row]
       {
        cell.date.text = convertDateFormater(x.updatedOn ?? "")
        cell.mobile.text = "Mob: \(x.driverMobile ?? "")"
        cell.lic.text = "Lic No: \(x.driverLicense ?? "")"
        cell.name.text = x.driverName ?? ""
       }
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return driverList?.driversList?.count ?? 0
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
         {
         let titleForSwipe = "REMOVE"
        //let colorForTitle : UIColor = .systemRed
    
         let action = UIContextualAction(style: .destructive, title:  titleForSwipe, handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                 print("Delete")
            let alert = UIAlertController(title: "Delete Driver", message: "Are you sure", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Yes", style: .default) { (_) in
                self.makePostCallRemoveDriver(indexpath: indexPath)

            }
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
                 success(true)
            
             
             })
       //  action.backgroundColor = .yellow
        let actionApply = UIContextualAction(style: .normal, title:  "Edit", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                 print("Apply")
            let x = driverList?.driversList?[indexPath.row]
            selectedDriver = x
            self.performSegue(withIdentifier: "updateDriver", sender: nil)
            
             success(true)


                             
             })
     let actionMore = UIContextualAction(style: .normal, title:  "MORE", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
             print("more")
         success(true)


                         
         })
     actionMore.backgroundColor = .lightGray
     actionMore.image = UIImage(systemName: "ellipsis")
     actionApply.backgroundColor = .red
     action.backgroundColor = .systemRed
        actionApply.backgroundColor = .systemGreen
     actionApply.image = UIImage(systemName: "pencil.circle")
     action.image = UIImage(systemName: "trash.fill")

             return UISwipeActionsConfiguration(actions: [action , actionApply])
         }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updateDriver"
        {
            let vc = segue.destination as! UpdateDriver
            vc.driverData = selectedDriver
        }
    }
    
    func makePostCallRemoveDriver(indexpath: IndexPath) {
        
        let x = driverList?.driversList?[indexpath.row]

        let decoder = JSONDecoder()
        let json: [String: Any] = ["allflag":false,"battery_percentage":"","battery_type":"","bearing":"","currentposition":"","device_brand":"","device_manufracture":"","device_model":"","device_name":"","device_os":"","driver_address":"Kolathu","driver_id_primary":"\(x!.driverIDPrimary!)","driver_license":"\(x!.driverLicense!)","driver_license_expiry":"\(x!.driverLicenseExpiry!)","driver_mobile":"\(x!.driverMobile!)","driver_name":"\(x!.driverName!)","driver_password":"\(x!.driverPassword!)","isExpanded":false,"isSelected":false,"is_active":"\(x!.isActive!)","job_status":"","latitude":"","longitude":"","network_operator":"","network_type":"","phone_type":"","profile_image":"","speed":"","user_id":"\(x!.userID!)","vendor_optiondriver":"","code":0,"created_by":"\(x!.createdBy!)"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/deleteDriver")
        {
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(DeleteDriverJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            //self.visitorFavouriteData?.visitorList!.remove(at: indexpath.row)
                            self.driverList?.driversList!.remove(at: indexpath.row)
                            self.makeGetCall()
                            self.driverTableView.deleteRows(at: [indexpath], with: .top)
                            //self.makePostCallVisitorRecord(addressID: self.addressIdSelected!)
                            //self.historyTabel.deleteRows(at: [indexpath], with: .top)
                            let alert = UIAlertController(title: "Driver", message: "Successfully Deleted", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Driver", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

}

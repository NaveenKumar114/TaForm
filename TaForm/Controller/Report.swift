//
//  Report.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-08.
//

import UIKit
import Charts
class Report: UIViewController, ChartViewDelegate {
    @IBOutlet weak var driverCommissionChart: BarChartView!
 
    @IBOutlet weak var driverExpenseCommissionChart: LineChartView!
    @IBOutlet weak var managerReportChart: PieChartView!
    @IBOutlet weak var chargeTypeChart: PieChartView!
    @IBOutlet weak var accountReportChart: PieChartView!
    @IBOutlet weak var expenseTypeChart: PieChartView!
    var reportChartData : ReportChartJSON?
    var driverCommissionSelected : [Chart]?
    var chargeTypeSelected : [ChartExpenseRequest]?
    var expenseTypeSelected  : [ChartExpenseType]?
    var accountReportSElected : [ChartAccountLastmonthElement]?
    var managerReportSElected : [ChartAccountLastmonthElement]?
    var driverExpenseSelected : [ChartExpense]?

    override func viewDidLoad() {
        super.viewDidLoad()
        driverCommissionChart.delegate = self
        expenseTypeChart.delegate = self
        chargeTypeChart.delegate = self
        accountReportChart.delegate = self
        managerReportChart.delegate = self
        driverExpenseCommissionChart.delegate = self
        makeGetCallReport()
        self.title = "Report"
        
      //  setDataCount(10, range: UInt32(10.0))
    //    test()
    }

    func makeGetCallReport() {
        let decoder = JSONDecoder()
        
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/dashboardGraphs")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(ReportChartJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            self.reportChartData = nil
                            
                            print(loginBaseResponse as Any)
                            self.reportChartData = loginBaseResponse
                                setupChart()
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func setupChart()
    {
        if let x = reportChartData
        {
            driverCommissionSelected = x.chart
            driverCommissionChartUpdate()
            expenseTypeSelected  = x.chartExpenseTypeMonthly
            expenseTypeUpdate()
            chargeTypeSelected = x.chartExpenseRequestMonthly
            chargeTypeUpdate()
            accountReportSElected = x.chartAccountMonthly
            accountReportUpdate()
            managerReportSElected = x.chartManagerMonthly
            managerReportUpdate()
            driverExpenseSelected = x.chartExpenseMonthly
            driverExpenseChartUpdate()
        }
    }

    func driverExpenseChartUpdate()
    {
        if let driverCom = driverExpenseSelected
        {
            var barDatacom  = [ChartDataEntry]()
            var barDataexp  = [ChartDataEntry]()
            for x in 0 ..< driverCom.count
            {
                let com = Double(driverCom[x].sumCommision ?? "0") ?? 0.0
                let exp = Double(driverCom[x].totalExpense ?? "0") ?? 0.0

               // let y =  BarChartDataEntry(x: Double(x), y: exp ?? 0.0 , data: driverCom[x].taDriverName ?? "")
                let e = ChartDataEntry(x: Double(x), y: exp, data: driverCom[x].taDriverName ?? "")
                let c = ChartDataEntry(x: Double(x), y: com, data: driverCom[x].taDriverName ?? "")
                barDatacom.append(c)
                barDataexp.append(e)
                
            }
            let set1 = LineChartDataSet(entries: barDataexp, label: "Expense")
            set1.axisDependency = .left
            set1.setColor(UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1))
            set1.setCircleColor(.white)
            set1.lineWidth = 2
            set1.circleRadius = 3
            set1.fillAlpha = 65/255
            set1.fillColor = UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)
            set1.highlightColor = UIColor(red: 244/255, green: 117/255, blue: 117/255, alpha: 1)
            set1.drawCircleHoleEnabled = false
            
            let set2 = LineChartDataSet(entries: barDatacom, label: "Commission")
            set2.axisDependency = .right
            set2.setColor(.red)
            set2.setCircleColor(.white)
            set2.lineWidth = 2
            set2.circleRadius = 3
            set2.fillAlpha = 65/255
            set2.fillColor = .red
            set2.highlightColor = UIColor(red: 244/255, green: 117/255, blue: 117/255, alpha: 1)
            set2.drawCircleHoleEnabled = false

         
            
            //let data: LineChartData = [set1, set2, set3]
            let data = LineChartData(dataSets: [set1, set2])
            data.setDrawValues(false)
            data.setValueTextColor(.white)
            
            data.setValueFont(.systemFont(ofSize: 9))
            
            driverExpenseCommissionChart.data = data
            
            self.driverExpenseCommissionChart.legend.enabled = true
        }
    }
    func driverCommissionChartUpdate () {
        
            if let driverCom = driverCommissionSelected
            {
                var barData  = [BarChartDataEntry]()
                for x in 0 ..< driverCom.count
                {
                    let exp = Double(driverCom[x].sumCommision ?? "0")
                    let y =  BarChartDataEntry(x: Double(x), y: exp ?? 0.0 , data: driverCom[x].taDriverName ?? "")
                    //let set2 = BarChartDataSet(entries: y , label: driverCom[x].taDriverName ?? "")
                    //let set2 = BarChartDataSet(entries: y)

                    barData.append(y)
                    
                }
                let set2 = BarChartDataSet(entries: barData , label: "")
                    set2.colors = ChartColorTemplates.joyful()

                let data = BarChartData(dataSet: set2)
                    data.setValueFont(UIFont(name: "HelveticaNeue-Light", size: 10)!)
                    data.barWidth = 0.9
                data.setDrawValues(false)

                
                    driverCommissionChart.data = data
                self.driverCommissionChart.xAxis.drawGridLinesEnabled = false
                self.driverCommissionChart.leftAxis.drawLabelsEnabled = false
                self.driverCommissionChart.legend.enabled = false
            }
        
       
        
    }
    func expenseTypeUpdate () {
        if let driverCom = expenseTypeSelected
        {
            var barData  = [PieChartDataEntry]()
            for x in 0 ..< driverCom.count
            {
                let exp = Double(driverCom[x].totalExpense ?? "0")
                //let y =  BarChartDataEntry(x: Double(x), y: exp ?? 0.0 , data: driverCom[x].expenseType ?? "")
                let y =  PieChartDataEntry(value: exp ?? 0.0, data: driverCom[x].expenseType ?? "")
                //let set2 = BarChartDataSet(entries: y , label: driverCom[x].taDriverName ?? "")
                //let set2 = BarChartDataSet(entries: y)
                

                barData.append(y)
                
            }
            let set2 = PieChartDataSet(entries: barData, label: "Widget Types")
                set2.colors = ChartColorTemplates.joyful()

            let data = PieChartData(dataSet: set2)
            set2.colors = ChartColorTemplates.joyful()
            expenseTypeChart.data = data
            data.setDrawValues(false)

            expenseTypeChart.notifyDataSetChanged()

            self.expenseTypeChart.legend.enabled = false

        
        }

    }
    func chargeTypeUpdate () {
        if let driverCom = chargeTypeSelected
        {
            var barData  = [PieChartDataEntry]()
            for x in 0 ..< driverCom.count
            {
                let exp = Double(driverCom[x].count ?? "0")
                //let y =  BarChartDataEntry(x: Double(x), y: exp ?? 0.0 , data: driverCom[x].expenseType ?? "")
                let y =  PieChartDataEntry(value: exp ?? 0.0, data: driverCom[x].truckTypeName ?? "")
                //let set2 = BarChartDataSet(entries: y , label: driverCom[x].taDriverName ?? "")
                //let set2 = BarChartDataSet(entries: y)
                

                barData.append(y)
                
            }
            let set2 = PieChartDataSet(entries: barData, label: "Widget Types")
                set2.colors = ChartColorTemplates.joyful()

            let data = PieChartData(dataSet: set2)
            set2.colors = ChartColorTemplates.joyful()
            chargeTypeChart.data = data
            data.setDrawValues(false)

            chargeTypeChart.notifyDataSetChanged()

           // self.chargeTypeChart.xAxis.drawGridLinesEnabled = false
           // self.chargeTypeChart.leftAxis.drawLabelsEnabled = false
            self.chargeTypeChart.legend.enabled = false
        
        }

    }
    func accountReportUpdate () {
        if let driverCom = accountReportSElected
        {
            var barData  = [PieChartDataEntry]()
          
            var exp = Double(driverCom[0].verified ?? "0")
            //let y =  BarChartDataEntry(x: Double(x), y: exp ?? 0.0 , data: driverCom[x].expenseType ?? "")
            var y =  PieChartDataEntry(value: exp ?? 0.0, data: "Verified")
            //let set2 = BarChartDataSet(entries: y , label: driverCom[x].taDriverName ?? "")
            //let set2 = BarChartDataSet(entries: y)
            

            barData.append(y)
            
            exp = Double(driverCom[0].pending ?? "0")
            //let y =  BarChartDataEntry(x: Double(x), y: exp ?? 0.0 , data: driverCom[x].expenseType ?? "")
             y =  PieChartDataEntry(value: exp ?? 0.0, data: "Pending")
            barData.append(y)

            let set2 = PieChartDataSet(entries: barData, label: "")
                set2.colors = ChartColorTemplates.joyful()

            let data = PieChartData(dataSet: set2)
            set2.colors = ChartColorTemplates.joyful()
            accountReportChart.data = data
            data.setDrawValues(false)

            accountReportChart.notifyDataSetChanged()

           // self.chargeTypeChart.xAxis.drawGridLinesEnabled = false
           // self.chargeTypeChart.leftAxis.drawLabelsEnabled = false
            self.accountReportChart.legend.enabled = false
        
        }

    }
    func managerReportUpdate () {
        if let driverCom = managerReportSElected
        {
            var barData  = [PieChartDataEntry]()
          
            var exp = Double(driverCom[0].verified ?? "0")
            //let y =  BarChartDataEntry(x: Double(x), y: exp ?? 0.0 , data: driverCom[x].expenseType ?? "")
            var y =  PieChartDataEntry(value: exp ?? 0.0, data: "Verified")
            //let set2 = BarChartDataSet(entries: y , label: driverCom[x].taDriverName ?? "")
            //let set2 = BarChartDataSet(entries: y)
            

            barData.append(y)
            
            exp = Double(driverCom[0].pending ?? "0")
            //let y =  BarChartDataEntry(x: Double(x), y: exp ?? 0.0 , data: driverCom[x].expenseType ?? "")
             y =  PieChartDataEntry(value: exp ?? 0.0, data: "Pending")
            barData.append(y)

            let set2 = PieChartDataSet(entries: barData, label: "Widget Types")
                set2.colors = ChartColorTemplates.joyful()

            let data = PieChartData(dataSet: set2)
            set2.colors = ChartColorTemplates.joyful()
            managerReportChart.data = data
            data.setDrawValues(false)

            managerReportChart.notifyDataSetChanged()

           // self.chargeTypeChart.xAxis.drawGridLinesEnabled = false
           // self.chargeTypeChart.leftAxis.drawLabelsEnabled = false
            self.managerReportChart.legend.enabled = false
        
        }

    }

    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print("rr")
        let marker:BalloonMarker = BalloonMarker(color: UIColor.black, font: UIFont(name: "Helvetica", size: 12)!, textColor: UIColor.white, insets: UIEdgeInsets(top: 7.0, left: 7.0, bottom: 7.0, right: 7.0))
        //let i = Int(entry.x)
        //marker.setLabel((reportChartData?.chartall?[i].taDriverName)!)
        marker.minimumSize = CGSize(width: 75.0, height: 35.0)
    
        chartView.marker = marker
        
    }

    
    @IBAction func driverCommissionSegment(_ sender: Any) {
        let s = sender as! UISegmentedControl
        if let x = reportChartData
        {
            // chart = currrent month
            // chart seven  = lastmonth
            // chart all = year
        switch s.selectedSegmentIndex {
        case 0:
            driverCommissionSelected = x.chart
        case 1:
            driverCommissionSelected = x.chartSevan
        case 2:
            driverCommissionSelected = x.chartall
        default:
            print("err")
        }
            driverCommissionChartUpdate()
        }
    }
    
    @IBAction func chargeTypeSegment(_ sender: Any) {
        let s = sender as! UISegmentedControl
        if let x = reportChartData
        {
            // chart = currrent month
            // chart seven  = lastmonth
            // chart all = year
        switch s.selectedSegmentIndex {
        case 0:
            chargeTypeSelected  = x.chartExpenseRequestMonthly
        case 1:
            chargeTypeSelected  = x.chartExpenseRequestSevan
        case 2:
            chargeTypeSelected  = x.chartExpenseRequestYear
        default:
            print("err")
        }
            chargeTypeUpdate()
            
        }

    }
    @IBAction func expenseTypeSegment(_ sender: Any) {
        
        let s = sender as! UISegmentedControl
        if let x = reportChartData
        {
            // chart = currrent month
            // chart seven  = lastmonth
            // chart all = year
        switch s.selectedSegmentIndex {
        case 0:
            expenseTypeSelected  = x.chartExpenseTypeMonthly
        case 1:
            expenseTypeSelected  = x.chartExpenseTypeLastmonth
        case 2:
            expenseTypeSelected  = x.chartExpenseTypeYear
        default:
            print("err")
        }
        expenseTypeUpdate()
            
        }
    }
    @IBAction func accountReportSegment(_ sender: Any) {
        
        let s = sender as! UISegmentedControl
        if let x = reportChartData
        {
            // chart = currrent month
            // chart seven  = lastmonth
            // chart all = year
        switch s.selectedSegmentIndex {
        case 0:
            accountReportSElected = x.chartAccountMonthly
        case 1:
            accountReportSElected = x.chartAccountLastmonth
        case 2:
            accountReportSElected = x.chartAccountYear
        default:
            print("err")
        }
        accountReportUpdate()
            
        }
    }
    
    @IBAction func managerReportStatement(_ sender: Any) {
        
        let s = sender as! UISegmentedControl
        if let x = reportChartData
        {
            // chart = currrent month
            // chart seven  = lastmonth
            // chart all = year
        switch s.selectedSegmentIndex {
        case 0:
            managerReportSElected = x.chartManagerMonthly
        case 1:
            managerReportSElected = x.chartManagerLastmonth
        case 2:
            managerReportSElected = x.chartManagerYear
        default:
            print("err")
        }
        managerReportUpdate()
            
        }
    }
    @IBAction func driverExpenseCommission(_ sender: Any) {
        let s = sender as! UISegmentedControl
        if let x = reportChartData
        {
            // chart = currrent month
            // chart seven  = lastmonth
            // chart all = year
        switch s.selectedSegmentIndex {
        case 0:
            driverExpenseSelected = x.chartExpenseMonthly
        case 1:
            driverExpenseSelected = x.chartExpenseSevan
        case 2:
            driverExpenseSelected = x.chartExpenseAll
        default:
            print("err")
        }
            driverExpenseChartUpdate()

        }

    }
}

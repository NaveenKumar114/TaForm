//
//  NewBooking.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-02.
//

import UIKit
import StepIndicator

class NewBooking: UIViewController {

    @IBOutlet weak var stepLabelFive: UIStackView!
    @IBOutlet weak var stepLabel: UIStackView!
    @IBOutlet weak var newAndDraftButton: UIView!
    @IBOutlet weak var stepIndicator: StepIndicatorView!
    @IBOutlet weak var summaryDetailsContainer: UIView!
    @IBOutlet weak var expenseDetailsContainer: UIView!
    @IBOutlet weak var advanceDetailsContainer: UIView!
    @IBOutlet weak var nextButtonButton: UIButton!
    @IBOutlet weak var jobDetailsContainer: UIView!
    @IBOutlet weak var driverDetailsContainer: UIView!
    @IBOutlet weak var backButton: CurvedView!
    @IBOutlet weak var nextButton: CurvedView!
    @IBOutlet weak var viewForStepper: UIView!
    var driverDetails : DriverDetails?
    var jobDetails : JobDetails?
    var advanceDetails : AdvanceDetailsBooking?
    var expenseDetails : ExpenseDetails?
    var summaryDetails : SummaryDetails?
    var currentSelected = 0
    var updateTaForm : TaformList?
    var containerViewArray = [UIView]()
    var newFormWithoutAdvanceExpense = false
    override func viewDidLoad() {
        super.viewDidLoad()
        containerViewArray = [driverDetailsContainer , jobDetailsContainer , advanceDetailsContainer , expenseDetailsContainer , summaryDetailsContainer]
        stepIndicator.currentStep = 0
        backButton.isHidden = true
    backButton.layer.borderWidth = 1
        backButton.layer.borderColor = constantsUsedInApp.accentColor.cgColor
        backButton.layer.cornerRadius = 10
        nextButton.layer.borderWidth = 1
       // stepIndicator.numberOfSteps = 2

        if newFormWithoutAdvanceExpense
        {
           // nextButtonButton.isHidden = true
            stepLabelFive.isHidden = true
            newAndDraftButton.isHidden = true
            stepLabel.isHidden = false
            
        }
        else
        {
            nextButtonButton.isHidden = false
            stepLabelFive.isHidden = false
            newAndDraftButton.isHidden = true
            stepLabel.isHidden = true
            //stepIndicator.numberOfSteps = 2
        }
        nextButton.layer.borderColor = constantsUsedInApp.accentColor.cgColor
        nextButton.layer.cornerRadius = 10
        if let x =  updateTaForm
        {
            driverDetails?.taNumber.text = x.taTadynamicNumber ?? ""
            driverDetails?.dateLabel.text = convertDateFormater(x.taFormdate ?? "")
            driverDetails?.createBy.text = x.createby ?? ""
            summaryDetails?.editDAta = updateTaForm
            //driverDetails?.selectedDriver
            let d = DriversList(driverIDPrimary: x.taDriverID ?? "", vendorID: "", joinDate: "", vendorOptiondriver: "", categoryDriver: "", driverName: x.taDriverName ?? "", userID: "", createdBy: "", driverJobNumber: "", driverJobID: "", driverTruckID: "", subscriberID: "", driverEmail: "", driverPassword: "", driverMobile: "", driverAddress: "", driverNrc: "", driverNrcFile: "", profileImage: "", driverLicense: "", driverLicenseFile: "", driverLicenseExpiry: "", isActive: "", createdOn: "", driversListCreatedBy: "", updatedOn: "", updatedBy: "", tokenid: "", jobStatus: "", currentposition: "", latitude: "", longitude: "", speed: "", bearing: "", batteryType: "", batteryPercentage: "", networkType: "", networkOperator: "", phoneType: "", deviceName: "", deviceManufracture: "", deviceBrand: "", deviceOS: "", deviceModel: "", geofence: "")
            driverDetails?.selectedDriver = d
            driverDetails?.driverNameTextField.text = x.taDriverName ?? ""
            if let multijob = x.jobTaformList
            {
                print(x)
               // print(x.jobTaformList?.count)
                //print(x.jobTaformList)
                for i in multijob
                {
                    print("multi")

       
                    let z = nightStayData(startDate: i.overnightFromdate ?? "", endDate: i.overnightTodate ?? "", noOfDAys: i.overnightDays ?? "", amount: i.overnightAmount ?? "", totalAmount: i.overnightTotalAmount ?? "")
                    jobDetails?.nightStayData = z
                    let c = ClientsList(clientID: "", clientName: i.jobClientName ?? "", clientDetails: "", createdBy: "", createdID: "", createdDate: "", updatedBy: "", updatedDate: "")
                    let fromLocation = LocationsList(locationID: "", location: i.jobFrom ?? "", createdBy: "", createdID: "", createdDate: "", updatedBy: "", updatedDate: "")
                    let toLocation = LocationsList(locationID: "", location: i.jobTo ?? "", createdBy: "", createdID: "", createdDate: "", updatedBy: "", updatedDate: "")
                    let truckType = TruckTypeList(truckTypeID: "", truckTypeName: i.jobChargeType ?? "", createdBy: "", createdID: "", updatedBy: "", createdDate: "", updatedDate: "")
                    let trucknumber = TruckNumberList(truckNumberID: i.truckNumberid ?? "", truckNo: i.truckNumber ?? "", truckType: "", createdID: "", createBy: "", createDate: "", modifyBy: "", modifyDate: "")
                    let y = jobDetailsDataType(nightStay: z, date: convertDateFormater(i.jobDate ?? ""), jNo: i.jobNumber ?? "", client: c, from: fromLocation, to: toLocation, truckType: truckType, truckNumber: trucknumber, comission: i.jobCommision ?? "", remark: i.jobRemark ?? "", id: i.jobID ?? ""   , createBy : i.jobCreatedBy ?? "" ,  createDate:  i.jobCreatedDate ?? "", taformID: i.taFormID ?? "")
                    jobDetails?.multiJobArray.append(y)
                }
                jobDetails?.multiJobTableView.reloadData()
            }
            if let advance = x.advanceList
            {
                for i in advance
                {
                    let y = AdvanceDataType(date: convertDateFormater(i.advanceDate ?? ""), advance: i.advanceType ?? "", amount: i.advanceAmount ?? "", id: i.advanceID ?? "", createBy: i.advanceCreateBy ?? "", createDate: i.advanceCreatedDate ?? "", modifyby: i.advanceModifyDate ?? "", loginiD: i.loginID ?? "", modifyDate: i.advanceModifyDate ?? "", taformID: i.taFormID ?? "")
                    advanceDetails?.advanceArray.append(y)
                }
                advanceDetails?.advanceTAbleVIew.reloadData()
            }
            if let expense = x.expensesList
            {
                for i in expense
                {
                   
                    let y = ExpenseDataType(expenseType: i.expenseType ?? "", cash: i.expenseCash ?? "", card: i.expenseCard ?? "", notes: i.expenseRemark ?? "", id: i.expenseID ?? "", createBy: i.expenseCreateBy ?? "", createDate: i.expenseCreatedDate ?? "", modifyby: i.expenseModifyBy ?? "", loginiD: i.loginID ?? "", modifyDate: i.expenseModifyDate ?? "", taformID: i.taFormID ?? "", expenseTypeID: i.expenseTypeID ?? "")
                    expenseDetails?.expenseArray.append(y)
                }
                expenseDetails?.expenseTableView.reloadData()
            }
        }
    }
    
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "embedDriver"
        {
            print("sss")
            let vc = segue.destination as! DriverDetails
            driverDetails = vc
            if updateTaForm != nil
            {
                vc.isUpdate = true
            }
        }
        
        if segue.identifier == "embedJob"
        {
            print("sss")
            let vc = segue.destination as! JobDetails
            jobDetails = vc
        }
        if segue.identifier == "embedAdvance"
        {
            print("sss")
            let vc = segue.destination as! AdvanceDetailsBooking
            advanceDetails = vc
        }

        if segue.identifier == "embedExpense"
        {
            print("sss")
            let vc = segue.destination as! ExpenseDetails
            expenseDetails = vc
        }
        if segue.identifier == "embedSummary"
        {
            print("sss")
            let vc = segue.destination as! SummaryDetails
            summaryDetails = vc
        }

    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        print(currentSelected)
        nextButton.isHidden = false
        var empty = true
        switch currentSelected {
        case 0:
            empty = driverDetails!.isEntriesEmpty()
        case 1:
            if newFormWithoutAdvanceExpense
            {
                
                
                empty = jobDetails!.isEntriesEmpty()

                if empty
                {
                    let alert = UIAlertController(title: "New Booking", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
                    
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                }
                else{
                let selectClient = jobDetails?.selectedClient
                let tadata = taformList(clientID: selectClient?.clientID!, clientName: selectClient?.clientName, commission: jobDetails?.comissionTextField.text ?? "0.0", jobDate: jobDetails?.jobDateTextField.text!, jobFrom: jobDetails?.selectedFrom?.location, fromId: jobDetails?.selectedFrom?.locationID, jobNumber: jobDetails?.jobNumberTextField.text!, jobRemark: jobDetails?.remarksTextField.text ?? "", jobTo: jobDetails?.selectedTo?.location, toID: jobDetails?.selectedTo?.locationID, truckNumber: jobDetails?.selectedTruckNUmber?.truckNo, truckNumberID: jobDetails?.selectedTruckNUmber?.truckNumberID, truckType: jobDetails?.selectedTRuckType?.truckTypeName, truckTyeID: jobDetails?.selectedTRuckType?.truckTypeID, nightStay: jobDetails?.nightStayData, taDate: driverDetails?.dateLabel.text!, taNumber: driverDetails?.taNumber.text!, driverDetails: (driverDetails?.selectedDriver)!)
             //   summaryDetails?.advanceArray = advanceDetails!.advanceArray
               // summaryDetails?.expenseArray = expenseDetails!.expenseArray
                summaryDetails?.commision = jobDetails?.comissionTextField.text //it will be calculated for multijob in summary details
                summaryDetails?.nightStay = jobDetails?.nightStayData //it will be calculated for multijob in summary details
                summaryDetails?.totalComission = jobDetails?.comissionTextField.text
                summaryDetails?.formData = tadata
                summaryDetails?.multiJobDetails = jobDetails!.multiJobArray
                summaryDetails?.calculate()
               // summaryDetails?.prepareForSubmitWithNoAdvacneEXpense(type: "COMPLETE")
                
                empty = false
                }
            }
            else
            {
            empty = jobDetails!.isEntriesEmpty()
            }
        case 2 :
            empty = advanceDetails!.isEntriesEmpty()
        case 3 :
            empty = expenseDetails!.isEntriesEmpty()
            if !empty
            {
                if updateTaForm != nil
                {
                    summaryDetails?.isUpdate = true
                    summaryDetails?.deletedJobIds = jobDetails!.deletedjobIDs
                    summaryDetails?.deletedAdvanceIDS = advanceDetails!.deletedAdvacneIDs
                    summaryDetails?.deletedExpenseIds = expenseDetails!.deletedsExpenseIDs

                }
            let selectClient = jobDetails?.selectedClient
            let tadata = taformList(clientID: selectClient?.clientID!, clientName: selectClient?.clientName, commission: jobDetails?.comissionTextField.text ?? "0.0", jobDate: jobDetails?.jobDateTextField.text!, jobFrom: jobDetails?.selectedFrom?.location, fromId: jobDetails?.selectedFrom?.locationID, jobNumber: jobDetails?.jobNumberTextField.text!, jobRemark: jobDetails?.remarksTextField.text ?? "", jobTo: jobDetails?.selectedTo?.location, toID: jobDetails?.selectedTo?.locationID, truckNumber: jobDetails?.selectedTruckNUmber?.truckNo, truckNumberID: jobDetails?.selectedTruckNUmber?.truckNumberID, truckType: jobDetails?.selectedTRuckType?.truckTypeName, truckTyeID: jobDetails?.selectedTRuckType?.truckTypeID, nightStay: jobDetails?.nightStayData, taDate: driverDetails?.dateLabel.text!, taNumber: driverDetails?.taNumber.text!, driverDetails: (driverDetails?.selectedDriver)!)
            summaryDetails?.advanceArray = advanceDetails!.advanceArray
            summaryDetails?.expenseArray = expenseDetails!.expenseArray
            summaryDetails?.commision = jobDetails?.comissionTextField.text
            summaryDetails?.nightStay = jobDetails?.nightStayData
            summaryDetails?.totalComission = jobDetails?.comissionTextField.text
            summaryDetails?.formData = tadata
            summaryDetails?.multiJobDetails = jobDetails!.multiJobArray
            summaryDetails?.calculate()
            }
        case 4 :
            empty = false
        default:
            print("error")
        }
        if empty
        {
            let alert = UIAlertController(title: "New Booking", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
        if newFormWithoutAdvanceExpense && currentSelected == 1
        {
            print("ss")
        }
        else
        {
            if newFormWithoutAdvanceExpense && currentSelected == 0
            {
                nextButtonButton.isHidden = true
                newAndDraftButton.isHidden = false
            }
        backButton.isHidden = false
        if currentSelected == 4
        {
            nextButton.isHidden = true
            //nextButton.setTitle("Submit", for: .normal)
    //        summaryDetails?.prepareForSubmit()
        }
        else
        {
            currentSelected += 1
            print(currentSelected)

            if currentSelected == 4
            {
                nextButton.isHidden = true

                //nextButton.setTitle("Submit", for: .normal)
            }

            stepIndicator.currentStep = currentSelected
            for n in 0 ... 4
            {
                if currentSelected == n
                {
                    containerViewArray[n].isHidden = false
                }
                else
                {
                    containerViewArray[n].isHidden = true
                }
            }
        }
        }
    }
        
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        nextButton.isHidden = false

        
        if newFormWithoutAdvanceExpense && currentSelected == 1
        {
            nextButtonButton.isHidden = false
            newAndDraftButton.isHidden = true
        }
        nextButton.setTitle("Next", for: .normal)
        if currentSelected == 0
        {
            backButton.isHidden = true
        }
        else
        {
            currentSelected -= 1
            stepIndicator.currentStep = currentSelected
            for n in 0 ... 4
            {
                if currentSelected == n
                {
                    containerViewArray[n].isHidden = false
                }
                else
                {
                    containerViewArray[n].isHidden = true
                }
            }
        }
       
    }

    @IBAction func draftBUttonPressed(_ sender: Any) { // pending button
       var empty = jobDetails!.isEntriesEmpty()

        if empty
        {
            let alert = UIAlertController(title: "New Booking", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else{
            if updateTaForm != nil
            {
                summaryDetails?.isUpdate = true
                summaryDetails?.deletedJobIds = jobDetails!.deletedjobIDs
             //   summaryDetails?.deletedAdvanceIDS = advanceDetails!.deletedAdvacneIDs
               // summaryDetails?.deletedExpenseIds = expenseDetails!.deletedsExpenseIDs

            }
        let selectClient = jobDetails?.selectedClient
        let tadata = taformList(clientID: selectClient?.clientID!, clientName: selectClient?.clientName, commission: jobDetails?.comissionTextField.text ?? "0.0", jobDate: jobDetails?.jobDateTextField.text!, jobFrom: jobDetails?.selectedFrom?.location, fromId: jobDetails?.selectedFrom?.locationID, jobNumber: jobDetails?.jobNumberTextField.text!, jobRemark: jobDetails?.remarksTextField.text ?? "", jobTo: jobDetails?.selectedTo?.location, toID: jobDetails?.selectedTo?.locationID, truckNumber: jobDetails?.selectedTruckNUmber?.truckNo, truckNumberID: jobDetails?.selectedTruckNUmber?.truckNumberID, truckType: jobDetails?.selectedTRuckType?.truckTypeName, truckTyeID: jobDetails?.selectedTRuckType?.truckTypeID, nightStay: jobDetails?.nightStayData, taDate: driverDetails?.dateLabel.text!, taNumber: driverDetails?.taNumber.text!, driverDetails: (driverDetails?.selectedDriver)!)
     //   summaryDetails?.advanceArray = advanceDetails!.advanceArray
       // summaryDetails?.expenseArray = expenseDetails!.expenseArray
        summaryDetails?.commision = jobDetails?.comissionTextField.text //it will be calculated for multijob in summary details
        summaryDetails?.nightStay = jobDetails?.nightStayData //it will be calculated for multijob in summary details
        summaryDetails?.totalComission = jobDetails?.comissionTextField.text
        summaryDetails?.formData = tadata
        summaryDetails?.multiJobDetails = jobDetails!.multiJobArray
        summaryDetails?.calculate()
            if updateTaForm != nil
            {
                summaryDetails?.prepareFOrSubmitUpdateTaformWitoutAdvacneExpense(type: "PENDING")
            }
            else
            {

        summaryDetails?.prepareForSubmitWithNoAdvacneEXpense(type: "PENDING")
            }
        empty = false
        }

    }
    @IBAction func submitButton(_ sender: Any) { //Assign button
        
        var empty = jobDetails!.isEntriesEmpty()

        if empty
        {
            let alert = UIAlertController(title: "New Booking", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else{
            if updateTaForm != nil
            {
                summaryDetails?.isUpdate = true
                summaryDetails?.deletedJobIds = jobDetails!.deletedjobIDs
                //summaryDetails?.deletedAdvanceIDS = advanceDetails!.deletedAdvacneIDs
                //summaryDetails?.deletedExpenseIds = expenseDetails!.deletedsExpenseIDs

            }
        let selectClient = jobDetails?.selectedClient
        let tadata = taformList(clientID: selectClient?.clientID!, clientName: selectClient?.clientName, commission: jobDetails?.comissionTextField.text ?? "0.0", jobDate: jobDetails?.jobDateTextField.text!, jobFrom: jobDetails?.selectedFrom?.location, fromId: jobDetails?.selectedFrom?.locationID, jobNumber: jobDetails?.jobNumberTextField.text!, jobRemark: jobDetails?.remarksTextField.text ?? "", jobTo: jobDetails?.selectedTo?.location, toID: jobDetails?.selectedTo?.locationID, truckNumber: jobDetails?.selectedTruckNUmber?.truckNo, truckNumberID: jobDetails?.selectedTruckNUmber?.truckNumberID, truckType: jobDetails?.selectedTRuckType?.truckTypeName, truckTyeID: jobDetails?.selectedTRuckType?.truckTypeID, nightStay: jobDetails?.nightStayData, taDate: driverDetails?.dateLabel.text!, taNumber: driverDetails?.taNumber.text!, driverDetails: (driverDetails?.selectedDriver)!)
     //   summaryDetails?.advanceArray = advanceDetails!.advanceArray
       // summaryDetails?.expenseArray = expenseDetails!.expenseArray
        summaryDetails?.commision = jobDetails?.comissionTextField.text //it will be calculated for multijob in summary details
        summaryDetails?.nightStay = jobDetails?.nightStayData //it will be calculated for multijob in summary details
        summaryDetails?.totalComission = jobDetails?.comissionTextField.text
        summaryDetails?.formData = tadata
        summaryDetails?.multiJobDetails = jobDetails!.multiJobArray
        summaryDetails?.calculate()
        if updateTaForm != nil
        {
            summaryDetails?.prepareFOrSubmitUpdateTaformWitoutAdvacneExpense(type: "ASSIGNED")
        }
        else
        {
        summaryDetails?.prepareForSubmitWithNoAdvacneEXpense(type: "ASSIGNED")
        }
        
        empty = false
        }

    }
}

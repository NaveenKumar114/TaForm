//
//  AddTruckNUmber.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-07.
//

import UIKit

class AddTruckNUmber: UIViewController, UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    var textfieldPickers = [UITextField]()
    var trucktypeJson : TruckTypeJSON?
    @IBOutlet weak var truckNumber: UITextField!
    @IBOutlet weak var truckType: UITextField!
    var delegate : addTruckNUmberProtocol?
    var editData : TruckNumberList?
    override func viewDidLoad() {
        super.viewDidLoad()
        makeGetCall()
        truckNumber.delegate = self
            //truckType.delegate = self
        truckNumber.tag = 100
        if let x = editData
        {
            truckNumber.text = x.truckNo ?? ""
            truckType.text = x.truckType ?? ""
            self.title = "Update Truck Number"
        }
        textfieldPickers = [truckType]
        for textField in 0 ... textfieldPickers.count - 1
        {
            textfieldPickers[textField].delegate = self
            createPickerView(textField: textfieldPickers[textField], tag: textField)
            //textfieldPickers[textField].font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        dismissPickerView()
    }
    
    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in textfieldPickers
        {
            textField.inputAccessoryView = toolBar
        }
        
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    @IBAction func submitButton(_ sender: Any) {
        if truckNumber.text == "" || truckType.text == ""
        {
            let alert = UIAlertController(title: "CLient", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if editData != nil
            {
                //let name = UserDefaults.standard.string(forKey: "name")
                
             //   let roletype = UserDefaults.standard.string(forKey: "loginrole")
                let id = UserDefaults.standard.string(forKey: "loginID")
                let json : [String : Any] = ["truck_no":"\(truckNumber.text!)","truck_number_id":"\(editData?.truckNumberID ?? "0")","truck_type":"\(truckType.text!)","code":0,"create_by":"\(editData?.createBy ?? "")","login_id":"\(id!)"]
                print(json)
                makePostCall(json: json, url: "\(constantsUsedInApp.baseUrl)taform/save_updateTrucknumber")
            }
            else
            {
            let name = UserDefaults.standard.string(forKey: "name")
            
         //   let roletype = UserDefaults.standard.string(forKey: "loginrole")
            let id = UserDefaults.standard.string(forKey: "loginID")
                let json : [String : Any] = ["truck_no":"\(truckNumber.text!)","truck_number_id":"0","truck_type":"\(truckType.text!)","code":0,"create_by":"\(name!)","login_id":"\(id!)"]
            makePostCall(json: json, url: "\(constantsUsedInApp.baseUrl)taform/save_updateTrucknumber")
            }
        }
    }
    func makeGetCall() {
        let decoder = JSONDecoder()
        
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllTrucktypes")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TruckTypeJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            self.trucktypeJson = nil
                            print(loginBaseResponse as Any)
                            self.trucktypeJson = loginBaseResponse
                            
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Location", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    @IBAction func cancelButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func makePostCall(json: [String: Any] , url : String) {
        let decoder = JSONDecoder()
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(url)")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TaSaveJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                    
                            let alert = UIAlertController(title: "Client", message: "\(loginBaseResponse?.response ?? "Saved")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                self.delegate?.refresh()
                                self.navigationController?.popViewController(animated: true)
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Client", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

}
extension AddTruckNUmber : UIPickerViewDelegate , UIPickerViewDataSource
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textfieldPickers[0].text = trucktypeJson?.truckTypeList?[0].truckTypeName ?? ""

        default:
            print("err")
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return trucktypeJson?.truckTypeList?.count ?? 0
        case 1:
            return 3
        case 2:
            return 2
        case 3:
            return 3
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
        //    selectedDriver = driverList?.driversList?[0]

            return trucktypeJson?.truckTypeList?[row].truckTypeName ?? ""
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            textfieldPickers[0].text = trucktypeJson?.truckTypeList?[row].truckTypeName ?? ""
        
        default:
            print("err")
            print(pickerView.tag)
        }
    }
}







protocol addTruckNUmberProtocol {
    func refresh()
}

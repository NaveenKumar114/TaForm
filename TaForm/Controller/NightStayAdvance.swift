//
//  NightStayAdvance.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-08.
//

import UIKit
import DatePickerDialog

protocol ngihtStayProtocol {
    func closing(data : nightStayData)
}

class NightStayAdvance: UIViewController , UITextFieldDelegate {
    @IBOutlet weak var amountMyr: UILabel!
    
    @IBOutlet weak var totalAmountMyr: UILabel!
    @IBOutlet weak var viewForBlur: UIView!
    @IBOutlet weak var viewForBorder: UIView!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var noOfDays: UITextField!
    var delegate : ngihtStayProtocol?
    @IBOutlet weak var totalAmount: UITextField!
    @IBOutlet weak var amount: UITextField!
    var startDt : Date?
    var endDt : Date?
    var noDt : Int = 0
    var totalAmt : Double?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewForBorder.layer.borderWidth = 1
        viewForBorder.layer.borderColor = UIColor.lightGray.cgColor
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        startDate.addGestureRecognizer(gesture)
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        endDate.addGestureRecognizer(gesture2)
        amount.delegate = self
        dismissPickerViewForTextField(textField: amount)
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.frame
        viewForBlur.addSubview(blurEffectView)
        amountMyr.clipsToBounds = true
        amountMyr.layer.cornerRadius = 10
        amountMyr.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        totalAmountMyr.clipsToBounds = true
            totalAmountMyr.layer.cornerRadius = 10
        totalAmountMyr.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == amount
        {
            let y = Double(textField.text ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            textField.text = s
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == amount
        {
            if textField.text == "0.00"
            {
                textField.text = ""
            }
        }

        return true
    }
    func dismissPickerViewForTextField(textField : UITextField) {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
            textField.inputAccessoryView = toolBar
        
        
    }
    @objc func action() {
        view.endEditing(true)
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        var x = 0.00
        x =  Double(amount.text ?? "0") ?? 0.00
        print(x)
        print (x * Double(noDt))
        let s = String(format: "%.2f", x * Double(noDt))
        totalAmount.text = s
        totalAmt = x * Double(noDt)

    }

    @objc func showDateOrTime(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UILabel
        switch x {
        case startDate , endDate :
        datePickerTapped(sender: x!)
            print("date")

        default:
            print("error in time date")
        }
        
    }
    func datePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { [self] (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                sender.text = formatter.string(from: dt)
                if sender == endDate
                {
                    endDt = date
                    let x = endDt?.timeIntervalSince(startDt!)
                    noDt = Int(x!) / 60 / 60 / 24
                    noOfDays.text = String(noDt)
                }
                if sender == startDate
                {
                 startDt = date
                }
            }
        }
        
    }
    @IBAction func cancelButton(_ sender: Any) {
        navigationController?.popViewController(animated: false)
    }
    @IBAction func saveButton(_ sender: Any) {
        if startDt == nil || endDt == nil || totalAmt == nil
        {
            let alert = UIAlertController(title: "NightStay", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            delegate?.closing(data: nightStayData(startDate: startDate.text!, endDate: endDate.text!, noOfDAys: noOfDays.text!, amount: amount.text!, totalAmount: totalAmount.text!))
            navigationController?.popViewController(animated: false)

        }
    }
    
}

struct nightStayData {
    var startDate : String?
    var endDate : String?
    var noOfDAys : String?
    var amount : String?
    var totalAmount : String?
}

//
//  AdvanceDetailsBooking.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-02.
//

import UIKit
import DatePickerDialog
class AdvanceDetailsBooking: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var advanceTextField: UITextField!
    @IBOutlet weak var advanceTAbleVIew: UITableView!
    @IBOutlet weak var saveButton: UIButton!
    var deletedAdvacneIDs = [String] ()
    var advanceArray = [AdvanceDataType]()
    var textfieldPickers = [UITextField]()
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == amountTextField
        {
            let y = Double(textField.text ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            textField.text = s
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == amountTextField
        {
            if textField.text == "0.00"
            {
                textField.text = ""
            }
        }

        return true
    }

    var advanceTypes : AdvanceTypeJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        makeGetCall()
        amountTextField.delegate = self
        amountTextField.tag = 99
        dateLabel.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        dateLabel.addGestureRecognizer(gesture)
        saveButton.layer.cornerRadius = 10
        advanceTAbleVIew.delegate = self
        advanceTAbleVIew.dataSource = self
        advanceTAbleVIew.register(UINib(nibName: reuseNibIdentifier.advanceCell, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.advanceCell)
        textfieldPickers = [advanceTextField]
        for textField in 0 ... textfieldPickers.count - 1
        {
            textfieldPickers[textField].delegate = self
            createPickerView(textField: textfieldPickers[textField], tag: textField)
            //textfieldPickers[textField].font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        dismissPickerView()
        dismissPickerViewForTextField(textField: amountTextField)
    }
    func isEntriesEmpty() -> Bool
    {
        if advanceArray.count == 0
        {
            return true

        }
        else
        {
            return false
        }
    }
    func dismissPickerViewForTextField(textField : UITextField) {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
            textField.inputAccessoryView = toolBar
        
        
    }

    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in textfieldPickers
        {
            textField.inputAccessoryView = toolBar
        }
        
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    func makeGetCall() {
        let decoder = JSONDecoder()
      
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAdvanceList")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(AdvanceTypeJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.advanceTypes = loginBaseResponse
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    @IBAction func saveButtonPressed(_ sender: Any) {
        print("save")
        if dateLabel.text == "Start Date" || amountTextField.text == "" || advanceTextField.text == ""
        {
            let alert = UIAlertController(title: "New Booking", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let x = AdvanceDataType(date: dateLabel.text!, advance: advanceTextField.text!, amount: amountTextField.text!)
            advanceArray.append(x)
            print(x)
            print(advanceArray.count)
            advanceTAbleVIew.reloadData()
            dateLabel.text = "Start Date"
            amountTextField.text = ""
            advanceTextField.text = ""
        }
    }
    @objc func showDateOrTime(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UILabel
        switch x {
        case dateLabel :
        datePickerTapped(sender: x!)
            print("date")

        default:
            print("error in time date")
        }
        
    }

    func datePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                sender.text = formatter.string(from: dt)
                // print(formatter.string(from: dt))
            }
        }
        
    }
}



extension AdvanceDetailsBooking: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = advanceTAbleVIew.dequeueReusableCell(withIdentifier: reuseCellIdentifier.advanceCell) as! AdvanceTableCell
        let advance = advanceArray[indexPath.row]
        print(advance)
        cell.advanceLabel.text = advance.advance
        let y = Double(advance.amount )
        let s = String(format: "%.2f", y ?? 0.0)

        cell.amountLabel.text = s
        cell.dateLabel.text = advance.date
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return advanceArray.count
        
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
         {
        
            //  action.backgroundColor = .yellow
            let actionApply = UIContextualAction(style: .normal, title:  "Delete", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                      print("Apply")
                  success(true)
                if advanceArray[indexPath.row].id != nil
                {
                    deletedAdvacneIDs.append(advanceArray[indexPath.row].id!)
                }
                advanceArray.remove(at: indexPath.row)
                advanceTAbleVIew.reloadData()
                                  
                print(deletedAdvacneIDs.count)
                  })
       
          actionApply.backgroundColor = .systemRed
        //  actionApply.image = UIImage(systemName: "hand.tap.fill")

                  return UISwipeActionsConfiguration(actions: [actionApply])
            }


    
    
}

extension AdvanceDetailsBooking : UIPickerViewDelegate , UIPickerViewDataSource
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textfieldPickers[0].text = advanceTypes?.advanceList?[0].advanceName ?? ""

        default:
            print("err")
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return advanceTypes?.advanceList?.count ?? 0
        case 1:
            return 3
        case 2:
            return 2
        case 3:
            return 3
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return advanceTypes?.advanceList?[row].advanceName ?? ""
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            textfieldPickers[0].text = advanceTypes?.advanceList?[row].advanceName ?? ""
        
        default:
            print("err")
            print(pickerView.tag)
        }
    }
}








struct AdvanceDataType {
    var date : String
    var advance : String
    var amount : String
    var id : String?
    var createBy : String?
    var createDate : String?
    var modifyby : String?
    var loginiD : String?
    var modifyDate : String?
    var taformID : String?
}



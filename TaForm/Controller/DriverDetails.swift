//
//  DriverDetails.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-03.
//

import UIKit
import DatePickerDialog


class DriverDetails: UIViewController, UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBOutlet weak var createBy: UITextField!
    @IBOutlet weak var taNumber: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var driverNameTextField: UITextField!
    var textfieldPickers = [UITextField]()
    var driverList : DriverListJSON?
    var selectedDriver : DriversList?
    var isUpdate = false
    func isEntriesEmpty() -> Bool
    {
        if driverNameTextField.text == "" || dateLabel.text == "Select Date"
        {
            return true

        }
        else
        {
            return false
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        makeGetCall()
        
        dateLabel.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        dateLabel.addGestureRecognizer(gesture)
        textfieldPickers = [driverNameTextField]
        for textField in 0 ... textfieldPickers.count - 1
        {
            textfieldPickers[textField].delegate = self
            createPickerView(textField: textfieldPickers[textField], tag: textField)
            //textfieldPickers[textField].font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        dismissPickerView()
        let name = UserDefaults.standard.string(forKey: "name")
        createBy.text = name
        // Do any additional setup after loading the view.
    }
    
    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in textfieldPickers
        {
            textField.inputAccessoryView = toolBar
        }
        
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    @objc func showDateOrTime(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UILabel
        switch x {
        case dateLabel :
        datePickerTapped(sender: x!)
            print("date")

        default:
            print("error in time date")
        }
        
    }

    func makeGetCall() {
        let decoder = JSONDecoder()
      
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllDrivers")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(DriverListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.driverList = loginBaseResponse
                            if self.isUpdate == false
                            {
                                self.taNumber.text = loginBaseResponse?.tanumber ?? ""

                            }
                            
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func datePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                sender.text = formatter.string(from: dt)
                // print(formatter.string(from: dt))
            }
        }
        
    }
    
}

extension DriverDetails : UIPickerViewDelegate , UIPickerViewDataSource
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textfieldPickers[0].text = driverList?.driversList?[0].driverName ?? ""

        default:
            print("err")
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return driverList?.driversList?.count ?? 0
        case 1:
            return 3
        case 2:
            return 2
        case 3:
            return 3
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            selectedDriver = driverList?.driversList?[0]

            return driverList?.driversList?[row].driverName ?? ""
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            textfieldPickers[0].text = driverList?.driversList?[row].driverName ?? ""
            selectedDriver = driverList?.driversList?[row]
        
        default:
            print("err")
            print(pickerView.tag)
        }
    }
}







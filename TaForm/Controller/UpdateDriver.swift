//
//  UpdateDriver.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-15.
//

import UIKit
import DatePickerDialog
class UpdateDriver: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var name: UITextField!
    var driverData : DriversList?
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var mobileNO: UITextField!
    @IBOutlet weak var licenseEcpiry: UILabel!
    @IBOutlet weak var licenseNO: UITextField!
    override func viewDidLoad() {
            super.viewDidLoad()
        address.delegate = self
        name.delegate = self
        mobileNO.delegate = self
        licenseNO.delegate = self
        
        if let  x = driverData
        {
            address.text = x.driverAddress
            name.text = x.driverName
            mobileNO.text = x.driverMobile
            licenseEcpiry.text = x.driverLicenseExpiry
            licenseNO.text = x.driverLicense
        }
        licenseEcpiry.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        licenseEcpiry.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @objc func showDateOrTime(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UILabel
        switch x {
        case licenseEcpiry :
        datePickerTapped(sender: x!)
            print("date")

        default:
            print("error in time date")
        }
        
    }
    func datePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                sender.text = formatter.string(from: dt)
                // print(formatter.string(from: dt))
            }
        }
        
    }
  
    @IBAction func updateButton(_ sender: Any) {
        if name.text == "" || mobileNO.text == "" || address.text == "" || licenseNO.text == "" || licenseEcpiry.text == ""
        {
            let alert = UIAlertController(title: "Driver", message: "Please Enter All Details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
        makePostCallUpdateDriver()
        }
    }
    @IBAction func cancelButton(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    func makePostCallUpdateDriver() {
        let decoder = JSONDecoder()
      
        let json: [String: Any] = [ "allflag":false,"driver_address":"\(address.text!)","driver_id_primary":"\(driverData!.driverIDPrimary!)","driver_license":"\(licenseNO.text!)","driver_license_expiry":"\(licenseEcpiry.text!)","driver_mobile":"\(mobileNO.text!)","driver_name":"\(name.text!)","driver_password":"\(driverData!.driverPassword!)","isExpanded":false,"isSelected":false,"is_active":"\(driverData!.isActive!)","user_id":"\(driverData!.userID!)","code":0,"create_by":"\(driverData!.createdBy!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/save_updateDriver")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                  //  print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TaListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            //print(loginBaseResponse as Any)
                            let alert = UIAlertController(title: "Driver", message: "\(loginBaseResponse?.response ?? "Success")", preferredStyle: UIAlertController.Style.alert)
                            NotificationCenter.default.post(name: Notification.Name("reloadlist"), object: nil)
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                self.navigationController?.popViewController(animated: true)
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)

                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Driver", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}

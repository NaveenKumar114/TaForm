//
//  AddTruckType.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-07.
//

import UIKit

class AddTruckType: UIViewController, UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var truckType: UITextField!
    
    var delegate : addTruckTypeProtocol?
    var editData : TruckTypeList?
    override func viewDidLoad() {
        super.viewDidLoad()
        truckType.delegate = self
        if let x = editData
        {
            truckType.text = x.truckTypeName ?? ""
            self.title = "Update Truck Type"
        }
    }
    

    @IBAction func submitButton(_ sender: Any) {
        if truckType.text == ""
        {
            let alert = UIAlertController(title: "Truck Type", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if editData != nil
            {
                
             //   let roletype = UserDefaults.standard.string(forKey: "loginrole")
                let id = UserDefaults.standard.string(forKey: "loginID")
                let json : [String : Any] = ["truck_type_id":"\(editData?.truckTypeID ?? "")","truck_type_name":"\(truckType.text!)","code":0,"create_by":"\(editData?.createdBy ?? "")","login_id":"\(id!)"]
                print(json)
                makePostCall(json: json, url: "\(constantsUsedInApp.baseUrl)taform/save_updateTrucktype")
            }
            else
            {
            let name = UserDefaults.standard.string(forKey: "name")
            
         //   let roletype = UserDefaults.standard.string(forKey: "loginrole")
            let id = UserDefaults.standard.string(forKey: "loginID")
            let json : [String : Any] = ["truck_type_id":"0","truck_type_name":"\(truckType.text!)","code":0,"create_by":"\(editData?.createdBy ?? "")","login_id":"\(id!)"]
            makePostCall(json: json, url: "\(constantsUsedInApp.baseUrl)taform/save_updateTrucktype")
            }
        }
    }
    @IBAction func cancelButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func makePostCall(json: [String: Any] , url : String) {
        let decoder = JSONDecoder()
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(url)")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TaSaveJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                    
                            let alert = UIAlertController(title: "Truck Type", message: "\(loginBaseResponse?.response ?? "Saved")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                self.delegate?.refresh()
                                self.navigationController?.popViewController(animated: true)
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Truck Type", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

}

protocol addTruckTypeProtocol {
    func refresh()
}

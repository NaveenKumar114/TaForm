//
//  SummaryDetails.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-07.
//

import UIKit

class SummaryDetails: UIViewController , UITextFieldDelegate {
    @IBOutlet weak var totalCOmissionTextField: UITextField!
    
    @IBOutlet weak var amountGivenByDriverTextField: UITextField!
    @IBOutlet weak var totalExpenseTextField: UITextField!
    @IBOutlet weak var balanceTextField: UITextField!
    @IBOutlet weak var totalAdvanceTextField: UITextField!
    @IBOutlet weak var nightstayComissionTextField: UITextField!
    @IBOutlet weak var additionalComissionTextField: UITextField!
    @IBOutlet weak var payToDriverTextField: UITextField!
    
    @IBOutlet weak var additionalCommisionNotes: UITextField!
    @IBOutlet weak var nightStayComossionNotesTextField: UITextField!
    @IBOutlet weak var totalComissionNotesTextField: UITextField!
    var expenseArray = [ExpenseDataType]()
    var advanceArray = [AdvanceDataType]()
    var totaladvance = 0.0
    var totalexpense = 0.0
    var totalNightStayComission = 0.0
    var balance = 0.0
    var commision : String?
    var nightStay : nightStayData?
    var totalComission : String?
    var formData : taformList?
    var totalCash = 0.0
    var totalCard = 0.0
    var driverDetails : DriversList?
    var multiJobDetails = [jobDetailsDataType]()
    var totalNightStayDays = 0
    var totCOm = 0.0
    var isUpdate = false
    var editDAta : TaformList?
    var deletedAdvanceIDS = [String] ()
    var deletedExpenseIds = [String] ()
    var deletedJobIds = [String] ()
    override func viewDidLoad() {
        super.viewDidLoad()
        amountGivenByDriverTextField.delegate = self
        additionalComissionTextField.delegate = self
        dismissPickerViewForTextField(textField: amountGivenByDriverTextField)
        dismissPickerViewForTextField(textField: additionalComissionTextField)
        additionalCommisionNotes.delegate = self
        totalComissionNotesTextField.delegate = self
        nightStayComossionNotesTextField.delegate = self
    }
    func dismissPickerViewForTextField(textField : UITextField) {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
            textField.inputAccessoryView = toolBar
        
        
    }
    @objc func action() {
        view.endEditing(true)
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField == amountGivenByDriverTextField || textField == additionalComissionTextField
        {
            let y = Double(textField.text ?? "0.0")
            let s = String(format: "%.2f", y ?? 0.0)
            textField.text = s
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == amountGivenByDriverTextField || textField == additionalComissionTextField
        {
            if textField.text == "0.00"
            {
                textField.text = ""
            }
        }

        return true
    }


    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == amountGivenByDriverTextField
        {
        print(balance)
        var x = 0.0
        x =  Double(amountGivenByDriverTextField.text ?? "0") ?? 0.0
        print(x)
            var y = 0.0

            y =  Double(additionalComissionTextField.text ?? "0") ?? 0.0
            let z = balance + x + y + totCOm + totalNightStayComission
        payToDriverTextField.text = (String(format: "%.2f", z))
        }
        if textField == additionalComissionTextField
        {
            var x = 0.0
            x =  Double(amountGivenByDriverTextField.text ?? "0") ?? 0.0
            print(x)
            var y = 0.0
            y =  Double(additionalComissionTextField.text ?? "0") ?? 0.0
            print(totCOm)
            let z = (x + balance) + y + totCOm + totalNightStayComission
            payToDriverTextField.text = (String(format: "%.2f", z))

        }

    }
    func calculate()
    {
         totaladvance = 0.0
         totalexpense = 0.0
         totalNightStayComission = 0.0
         balance = 0.0
         commision  = ""
        totalComission = ""
         totalCash = 0.0
         totalCard = 0.0
       
         totalNightStayDays = 0
        for x in advanceArray
        {
            let y = Double(x.amount)
            totaladvance = totaladvance + (y ?? 0.0)
        }
        totalAdvanceTextField.text = (String(format: "%.2f", totaladvance))
        for x in expenseArray
        {
            let y = Double(x.cash)
            totalexpense = totalexpense + (y ?? 0.0)
            totalCash = totalCash + (y ?? 0.0)
            if let z = Double(x.card)
            {
                totalCard = totalCard + z
            }
        }
        for x in multiJobDetails
        {
            if let z = x.comission
            {
                totCOm = totCOm + Double(z)!
               
            }
            if let n = x.nightStay
            {
                if let y = Int(n.noOfDAys ?? "0")
                {
                    totalNightStayDays = totalNightStayDays + y
                }
                if let y = Double(n.totalAmount ?? "0.0")
                {
                totalNightStayComission = totalNightStayComission + y
                }
            }
           
        }
        totalCOmissionTextField.text = (String(format: "%.2f", totCOm))
        totalExpenseTextField.text = (String(format: "%.2f", totalexpense))
        balance = (totalexpense - totaladvance)
        let x = balance + totCOm + totalNightStayComission
        balanceTextField.text = (String(format: "%.2f", balance))
        payToDriverTextField.text = (String(format: "%.2f", x))
        nightstayComissionTextField.text = (String(format: "%.2f", totalNightStayComission))
        nightStayComossionNotesTextField.text = "\(String(totalNightStayDays)) Days"
    }
    func createAdvanceList() -> [[String : Any]]
    {
        var advance = [[String : Any]]()
        if isUpdate
        {
            let id = UserDefaults.standard.string(forKey: "loginID")
            let name = UserDefaults.standard.string(forKey: "name")
            let date = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    formatter.timeZone = TimeZone(abbreviation: "UTC")
            let defaultTimeZoneStr = formatter.string(from: date as Date)

            for x in advanceArray
            {
              

               
                let y : [String : Any] = [  "advance_amount": "\(x.amount)",
                                            "advance_create_by": "\(x.createBy ?? "")",
                                            "advance_created_date": "\(x.createDate ?? "")",
                           "advance_date": "\(x.date)",
                           "advance_id": "\(x.id ?? "0")",
                           "advance_modify_by": "\(name!)",
                           "advance_modify_date": "\(defaultTimeZoneStr)",
                           "advance_type": "\(x.advance )",
                           "ta_form_id": "\(x.taformID ?? "")",
                           "code": 0,
                           "login_id": "\(x.loginiD ?? id!)"]
                advance.append(y)
            }
        }
        else
        {
        for x in advanceArray
        {
            let y = ["advance_amount":"\(x.amount)","advance_type":"\(x.advance)","advance_date":"\(x.date)"]
            advance.append(y)
        }
        }
        return advance
    }
    func createExpenseList() -> [[String : Any]]
    {
        var expense = [[String : Any]]()
        if isUpdate
        {
            let id = UserDefaults.standard.string(forKey: "loginID")
            let name = UserDefaults.standard.string(forKey: "name")
            let date = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    formatter.timeZone = TimeZone(abbreviation: "UTC")
            let defaultTimeZoneStr = formatter.string(from: date as Date)

            for x in expenseArray
            {
                let y : [String : Any] = [  "expense_card": "\(x.card)",
                                            "expense_cash": "\(x.cash)",
                                            "expense_create_by": "\(x.createBy ?? "")",
                                            "expense_created_date": "\(x.createDate ?? "")",
                                            "expense_id": "\(x.id ?? "0")",
                                            "expense_modify_by": "\(name!)",
                                            "expense_modify_date": "\(defaultTimeZoneStr)",
                                            "expense_remark": "\(x.notes)",
                                            "expense_type": "\(x.expenseType )",
                                            "expense_type_id": "\(x.expenseTypeID ?? "" )",
                                            "login_id": "\(x.loginiD ?? id!)",
                                            "ta_form_id": "\(x.taformID ?? "0")"
]
                expense.append(y)
            }

        }
        else
        {
        for x in expenseArray
        {
            let y = ["expense_card":"\(x.card)","expense_cash":"\(x.cash)","expense_type":"\(x.expenseType)","expense_remark":"\(x.notes)"]
            expense.append(y)
        }
        }
        return expense
    }
    func createJobList() -> [[String : Any]]
    {
        var jobs = [[String : Any]]()
        if isUpdate
        {
            let id = UserDefaults.standard.string(forKey: "loginID")
            let name = UserDefaults.standard.string(forKey: "name")
            let date = NSDate()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //    formatter.timeZone = TimeZone(abbreviation: "UTC")
            let defaultTimeZoneStr = formatter.string(from: date as Date)
            for x in multiJobDetails
            {
                var night = "NO"
                if x.nightStay == nil
                {
                    night = "NO"
                }
                else
                {
                    night = "YES"
                }
               
             
                let y : [String : Any] = ["job_charge_type": "\(x.truckType?.truckTypeName ?? "")",
                         "job_client_name": "\(x.client?.clientName ?? "")",
                         "job_commision": "\(x.comission ?? "0.0")",
                         "job_created_by": "\(x.createBy ?? "")",
                         "job_created_date": "\(x.createDate ?? "")",
                         "job_date": "\(x.date ?? "")",
                         "job_from": "\(x.from?.location ?? "")",
                         "job_id": "\(x.id ?? "0")",
                         "job_modify_by": "\(name!)",
                         "job_modify_date": "\(defaultTimeZoneStr)",
                         "job_number": "\(x.jNo ?? "")",
                         "job_remark": "\(x.remark ?? "")",
                         "job_status": "",
                         "job_to": "\(x.to?.location ?? "")",
                         "overnight_amount": "\(x.nightStay?.amount ?? "")",
                         "overnight_days": "\(x.nightStay?.noOfDAys ?? "")",
                         "overnight_fromdate": "\(x.nightStay?.startDate ?? "")",
                         "overnight_notes": "",
                         "overnight_todate": "\(x.nightStay?.endDate ?? "")",
                         "overnight_total_amount": "\(x.nightStay?.totalAmount ?? "")",
                         "overnightflag": "\(night)",
                         "status": "",
                         "ta_form_id": "\(x.taformID ?? "0")",
                         "truck_number": "\(x.truckNumber?.truckNo ?? "")",
                         "truck_numberid": "\(x.truckNumber?.truckNumberID ?? "")",
                         "code": 0,
                         "login_id": "\(id!)"
]
                jobs.append(y)
            }
        }
        else
        {
        for x in multiJobDetails
        {
            var night = "NO"
            if x.nightStay == nil
            {
                night = "NO"
            }
            else
            {
                night = "YES"
            }
           
            let y = ["job_client_id":"\(x.client?.clientID ?? "")","job_charge_type":"\(x.truckType?.truckTypeName ?? "")","job_charge_type_id":"\(x.truckType?.truckTypeID ?? "")","job_client_name":"\(x.client?.clientName ?? "")","job_commision":"\(x.comission ?? "0.0")","job_date":"\(x.date ?? "")","job_from":"\(x.from?.location ?? "")","job_from_id":"\(x.from?.locationID ?? "")","job_number":"\(x.jNo ?? "")","job_remark":"\(x.remark ?? "")","job_to":"\(x.to?.location ?? "")","job_to_id":"\(x.to?.locationID ?? "")","overnight_amount":"\(x.nightStay?.amount ?? "")","overnight_days":"\(x.nightStay?.noOfDAys ?? "")","overnight_fromdate":"\(x.nightStay?.startDate ?? "")","overnight_todate":"\(x.nightStay?.endDate ?? "")","overnight_total_amount":"\(x.nightStay?.totalAmount ?? "")","overnightflag":"\(night)","truck_number":"\(x.truckNumber?.truckNo ?? "")","truck_numberid":"\(x.truckNumber?.truckNumberID ?? "")"] as [String : Any]
            jobs.append(y)
        }
        }
        return jobs
    }
    func calculateTotalComission () -> String
    {
        var y = 0.0
        y =  Double(additionalComissionTextField.text ?? "0") ?? 0.0
        var x = 0.0
        x =  Double(totalCOmissionTextField.text ?? "0") ?? 0.0
        let z = x + y
        return String(z)

    }
    @IBAction func submitButton(_ sender: Any) {
        if isUpdate
        {
            prepareFOrSubmitUpdateTaform(type: "COMPLETE")

        }
        else
        {
        prepareForSubmit(type: "COMPLETE")
        }
    }
    @IBAction func draftButton(_ sender: Any) {
        if isUpdate
        {
        prepareFOrSubmitUpdateTaform(type: "DRAFT")
        }
        else
        {
        prepareForSubmit(type: "DRAFT")
        }

    }
    func prepareFOrSubmitUpdateTaformWitoutAdvacneExpense(type : String) // tajob
    {
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //    formatter.timeZone = TimeZone(abbreviation: "UTC")
        let defaultTimeZoneStr = formatter.string(from: date as Date)
        let advance = createAdvanceList()
        let expense = createExpenseList()
        let job = createJobList()
        let x = formData!
        let sumComission = calculateTotalComission()
        var y = 0.0
        y =  Double(nightstayComissionTextField.text ?? "0") ?? 0.0
        let sumOfComission = Double(sumComission) ?? 0.0 + y
        let name = UserDefaults.standard.string(forKey: "name")
        
        let roletype = UserDefaults.standard.string(forKey: "loginrole")
        let id = UserDefaults.standard.string(forKey: "loginID")
      
        let json : [String : Any] = [
            "account_duration": "",
            "account_status": "Pending",
            "account_status_by": "",
            "addtional_pickup_commsion": "\(additionalComissionTextField.text ?? "")",
            "addtional_pickup_commsion_remark": "\(additionalCommisionNotes.text ?? "")",
            "advanceList": [],
            "cancel_comment": "",
            "createby": "\(editDAta?.createby ?? "")",
            "createddate": "\(editDAta?.createddate ?? "")",
            "driver_id": "\(x.driverDetails.driverIDPrimary!)",
            "driver_name": "\(x.driverDetails.driverName!)",
            "editFlag": true,
            "expensesList": [] ,
            "isExpanded": false,
            "is_open_flag": "0",
            "jobTaformList": job,
            "job_total_advance": "\(totalAdvanceTextField.text ?? "")",
            "job_total_commission": "\(totalCOmissionTextField.text ?? "")",
            "login_role": "\(roletype!)",
            "manager_duration": "",
            "manager_status": "Pending",
            "manager_status_by": "",
            "modifyby": "\(name!)",
            "modifydate": "\(defaultTimeZoneStr)",
            "night_duty_commision": "\(nightstayComissionTextField.text ?? "")",
            "night_duty_commision_remark": "\(nightStayComossionNotesTextField.text ?? "0")",
            "open_by": "",
            "open_by_id": "0",
            "operation_duration": "",
            "operation_status": "Pending",
            "operation_status_by": "\(name!)",
            "operation_status_time": "\(defaultTimeZoneStr)",
            "reject_flag": "0",
            "reject_login_id": "0",
            "rejected_by": "",
            "rejected_comment": "",
            "rejected_ta_id": "0",
            "rejected_ta_number": "",
            "request_comment": "",
            "sequence": "\(editDAta?.sequence ?? "")",
            "sum_of_card_expense": "\(String(totalCard))",
            "sum_of_cash_expense": "\(String(totalCash))",
            "sum_of_job_commision": "\(sumComission)",
            "ta_date": "\(x.taDate! )",
            "ta_driver_id": "\(x.driverDetails.driverIDPrimary!)",
            "ta_driver_name": "\(x.driverDetails.driverName!)",
            "ta_form_id": "\(editDAta?.taFormID ?? "")",
            "ta_formdate": "\(x.taDate ?? "")",
            "ta_number": "\(x.taNumber!)",
            "ta_status": "\(type)",
            "ta_tadynamic_number": "\(editDAta?.taTadynamicNumber ?? x.taNumber!)",
            "total_additional_commission_amount": "\(additionalComissionTextField.text ?? "")",
            "total_additional_commission_amount_notes": "\(additionalCommisionNotes.text ?? "")",
            "total_advance": "\(totalAdvanceTextField.text ?? "")",
            "total_balance": "\(balanceTextField.text ?? "")",
            "total_card": "\(String(totalCard))",
            "total_cash": "\(String(totalCash))",
            "total_cash_spend": "\(String(totalCash))",
            "total_cash_to_company": "\(amountGivenByDriverTextField.text ?? "")",
            "total_cash_to_driver": "\(payToDriverTextField.text ?? "")",
            "total_commision": "\(totalCOmissionTextField.text ?? "")",
            "total_commission": "\(totalCOmissionTextField.text ?? "")" ,
            "total_commission_notes": "\(totalComissionNotesTextField.text ?? "")",
            "total_expensebycard": "\(String(totalCard))",
            "total_expensebycash": "\(String(totalCash))",
            "total_givenbydriver": "\(amountGivenByDriverTextField.text ?? "")",
            "total_job_commission": "\(totalCOmissionTextField.text ?? "")",
            "total_job_commission_remark": "\(totalComissionNotesTextField.text ?? "")",
            "total_jobadvance": "\(totalAdvanceTextField.text ?? "")",
            "total_jobadvanceadditional": "\(additionalComissionTextField.text ?? "")",
            "total_jobadvancenightstay": "\(nightstayComissionTextField.text ?? "")",
            "total_jobamountgivenbydriver": "\(amountGivenByDriverTextField.text ?? "")",
            "total_jobcommission": "\(totalCOmissionTextField.text ?? "")",
            "total_nightstay_commission_amount": "\(nightstayComissionTextField.text ?? "")",
            "total_nightstay_commission_amount_notes": "\(nightStayComossionNotesTextField.text ?? "0")",
            "total_paytodriver": "\(payToDriverTextField.text ?? "")",
            "total_sum_commision": "\(sumComission)",
            "total_sumof_commissions": "\(sumOfComission)",
            "code": 0,
            "create_by": "\(editDAta?.createby ?? name!)",
            "login_id": "\(id!)" ,
            "deletedAdvanceList":deletedAdvanceIDS ,"deletedExpenseList":deletedExpenseIds,"deletedJobList":deletedJobIds
          ]

        makePostCall(json: json, url: "\(constantsUsedInApp.baseUrl)taform/updateTaformJobs")
        print(json)
    }

    func prepareForSubmitWithNoAdvacneEXpense(type : String)
    {
     //   let advance = createAdvanceList()
       // let expense = createExpenseList()
        let job = createJobList()
        let x = formData!
        let sumComission = calculateTotalComission()
        var y = 0.0
        y =  Double(nightstayComissionTextField.text ?? "0") ?? 0.0
        let sumOfComission = Double(sumComission) ?? 0.0 + y
        let name = UserDefaults.standard.string(forKey: "name")
        
        let roletype = UserDefaults.standard.string(forKey: "loginrole")
        let id = UserDefaults.standard.string(forKey: "loginID")
        let json : [String : Any] = ["advanceList": [],"driver_id":"\(x.driverDetails.driverIDPrimary!)","driver_name":"\(x.driverDetails.driverName!)","editFlag":false,"expensesList": [],"isExpanded":false,"jobTaformList":job,"job_total_advance":"\(totalAdvanceTextField.text!)","job_total_commission":"\(totalCOmissionTextField.text!)","login_id":"\(id!)","operation_status":"Verified","operation_status_by":"\(name!)","ta_date":"\(x.taDate!)","ta_number":"\(x.taNumber!)","ta_status":"\(type)","total_additional_commission_amount":"\(additionalComissionTextField.text ?? "")","total_additional_commission_amount_notes":"\(additionalCommisionNotes.text ?? "")","total_advance":"\(totalAdvanceTextField.text ?? "")","total_balance":"\(balanceTextField.text ?? "")","total_card":"\(String(totalCard))","total_cash":"\(String(totalCash))","total_cash_to_company":"\(amountGivenByDriverTextField.text ?? "")","total_cash_to_driver":"\(payToDriverTextField.text ?? "")","total_commission":"\(totalCOmissionTextField.text ?? "")","total_commission_notes":"\(totalComissionNotesTextField.text ?? "")","total_expensebycard":"\(String(totalCard))","total_expensebycash":"\(String(totalCash))","total_jobadvance":"\(totalAdvanceTextField.text ?? "")","total_jobadvanceadditional":"\(additionalComissionTextField.text ?? "")","total_jobadvancenightstay":"\(nightstayComissionTextField.text ?? "")","total_jobamountgivenbydriver":"\(amountGivenByDriverTextField.text ?? "")","total_jobcommission":"\(totalCOmissionTextField.text ?? "")","total_nightstay_commission_amount":"\(nightstayComissionTextField.text ?? "")","total_nightstay_commission_amount_notes":"\(nightStayComossionNotesTextField.text ?? "0")","total_sum_commision":"\(sumComission)","total_sumof_commissions":"\(sumOfComission)","create_by":"\(name!)" , "login_role" : "\(roletype!)"]
        makePostCall(json: json, url: "\(constantsUsedInApp.baseUrl)taform/saveTaformJobs")
        print(json)
    }
    func prepareForSubmit(type : String)
    {
        let advance = createAdvanceList()
        let expense = createExpenseList()
        let job = createJobList()
        let x = formData!
        let sumComission = calculateTotalComission()
        var y = 0.0
        y =  Double(nightstayComissionTextField.text ?? "0") ?? 0.0
        let sumOfComission = Double(sumComission) ?? 0.0 + y
        let name = UserDefaults.standard.string(forKey: "name")
        
        let roletype = UserDefaults.standard.string(forKey: "loginrole")
        let id = UserDefaults.standard.string(forKey: "loginID")
        let json : [String : Any] = ["advanceList": advance,"driver_id":"\(x.driverDetails.driverIDPrimary!)","driver_name":"\(x.driverDetails.driverName!)","editFlag":false,"expensesList": expense,"isExpanded":false,"jobTaformList":job,"job_total_advance":"\(totalAdvanceTextField.text!)","job_total_commission":"\(totalCOmissionTextField.text!)","login_id":"\(id!)","operation_status":"Verified","operation_status_by":"\(name!)","ta_date":"\(x.taDate!)","ta_number":"\(x.taNumber!)","ta_status":"\(type)","total_additional_commission_amount":"\(additionalComissionTextField.text ?? "")","total_additional_commission_amount_notes":"\(additionalCommisionNotes.text ?? "")","total_advance":"\(totalAdvanceTextField.text ?? "")","total_balance":"\(balanceTextField.text ?? "")","total_card":"\(String(totalCard))","total_cash":"\(String(totalCash))","total_cash_to_company":"\(amountGivenByDriverTextField.text ?? "")","total_cash_to_driver":"\(payToDriverTextField.text ?? "")","total_commission":"\(totalCOmissionTextField.text ?? "")","total_commission_notes":"\(totalComissionNotesTextField.text ?? "")","total_expensebycard":"\(String(totalCard))","total_expensebycash":"\(String(totalCash))","total_jobadvance":"\(totalAdvanceTextField.text ?? "")","total_jobadvanceadditional":"\(additionalComissionTextField.text ?? "")","total_jobadvancenightstay":"\(nightstayComissionTextField.text ?? "")","total_jobamountgivenbydriver":"\(amountGivenByDriverTextField.text ?? "")","total_jobcommission":"\(totalCOmissionTextField.text ?? "")","total_nightstay_commission_amount":"\(nightstayComissionTextField.text ?? "")","total_nightstay_commission_amount_notes":"\(nightStayComossionNotesTextField.text ?? "0")","total_sum_commision":"\(sumComission)","total_sumof_commissions":"\(sumOfComission)","create_by":"\(name!)" , "login_role" : "\(roletype!)"]
        makePostCall(json: json, url: "\(constantsUsedInApp.baseUrl)taform/saveTaform")
        print(json)
    }
    func prepareFOrSubmitUpdateTaform(type : String)
    {
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //    formatter.timeZone = TimeZone(abbreviation: "UTC")
        let defaultTimeZoneStr = formatter.string(from: date as Date)
        let advance = createAdvanceList()
        let expense = createExpenseList()
        let job = createJobList()
        let x = formData!
        let sumComission = calculateTotalComission()
        var y = 0.0
        y =  Double(nightstayComissionTextField.text ?? "0") ?? 0.0
        let sumOfComission = Double(sumComission) ?? 0.0 + y
        let name = UserDefaults.standard.string(forKey: "name")
        
        let roletype = UserDefaults.standard.string(forKey: "loginrole")
        let id = UserDefaults.standard.string(forKey: "loginID")
      
        let json : [String : Any] = [
            "account_duration": "",
            "account_status": "Pending",
            "account_status_by": "",
            "addtional_pickup_commsion": "\(additionalComissionTextField.text ?? "")",
            "addtional_pickup_commsion_remark": "\(additionalCommisionNotes.text ?? "")",
            "advanceList": advance,
            "cancel_comment": "",
            "createby": "\(editDAta?.createby ?? "")",
            "createddate": "\(editDAta?.createddate ?? "")",
            "driver_id": "\(x.driverDetails.driverIDPrimary!)",
            "driver_name": "\(x.driverDetails.driverName!)",
            "editFlag": true,
            "expensesList": expense ,
            "isExpanded": false,
            "is_open_flag": "0",
            "jobTaformList": job,
            "job_total_advance": "\(totalAdvanceTextField.text ?? "")",
            "job_total_commission": "\(totalCOmissionTextField.text ?? "")",
            "login_role": "\(roletype!)",
            "manager_duration": "",
            "manager_status": "Pending",
            "manager_status_by": "",
            "modifyby": "\(name!)",
            "modifydate": "\(defaultTimeZoneStr)",
            "night_duty_commision": "\(nightstayComissionTextField.text ?? "")",
            "night_duty_commision_remark": "\(nightStayComossionNotesTextField.text ?? "0")",
            "open_by": "",
            "open_by_id": "0",
            "operation_duration": "",
            "operation_status": "Pending",
            "operation_status_by": "\(name!)",
            "operation_status_time": "\(defaultTimeZoneStr)",
            "reject_flag": "0",
            "reject_login_id": "0",
            "rejected_by": "",
            "rejected_comment": "",
            "rejected_ta_id": "0",
            "rejected_ta_number": "",
            "request_comment": "",
            "sequence": "\(editDAta?.sequence ?? "")",
            "sum_of_card_expense": "\(String(totalCard))",
            "sum_of_cash_expense": "\(String(totalCash))",
            "sum_of_job_commision": "\(sumComission)",
            "ta_date": "\(x.taDate! )",
            "ta_driver_id": "\(x.driverDetails.driverIDPrimary!)",
            "ta_driver_name": "\(x.driverDetails.driverName!)",
            "ta_form_id": "\(editDAta?.taFormID ?? "")",
            "ta_formdate": "\(x.taDate ?? "")",
            "ta_number": "\(x.taNumber!)",
            "ta_status": "\(type)",
            "ta_tadynamic_number": "\(editDAta?.taTadynamicNumber ?? x.taNumber!)",
            "total_additional_commission_amount": "\(additionalComissionTextField.text ?? "")",
            "total_additional_commission_amount_notes": "\(additionalCommisionNotes.text ?? "")",
            "total_advance": "\(totalAdvanceTextField.text ?? "")",
            "total_balance": "\(balanceTextField.text ?? "")",
            "total_card": "\(String(totalCard))",
            "total_cash": "\(String(totalCash))",
            "total_cash_spend": "\(String(totalCash))",
            "total_cash_to_company": "\(amountGivenByDriverTextField.text ?? "")",
            "total_cash_to_driver": "\(payToDriverTextField.text ?? "")",
            "total_commision": "\(totalCOmissionTextField.text ?? "")",
            "total_commission": "\(totalCOmissionTextField.text ?? "")" ,
            "total_commission_notes": "\(totalComissionNotesTextField.text ?? "")",
            "total_expensebycard": "\(String(totalCard))",
            "total_expensebycash": "\(String(totalCash))",
            "total_givenbydriver": "\(amountGivenByDriverTextField.text ?? "")",
            "total_job_commission": "\(totalCOmissionTextField.text ?? "")",
            "total_job_commission_remark": "\(totalComissionNotesTextField.text ?? "")",
            "total_jobadvance": "\(totalAdvanceTextField.text ?? "")",
            "total_jobadvanceadditional": "\(additionalComissionTextField.text ?? "")",
            "total_jobadvancenightstay": "\(nightstayComissionTextField.text ?? "")",
            "total_jobamountgivenbydriver": "\(amountGivenByDriverTextField.text ?? "")",
            "total_jobcommission": "\(totalCOmissionTextField.text ?? "")",
            "total_nightstay_commission_amount": "\(nightstayComissionTextField.text ?? "")",
            "total_nightstay_commission_amount_notes": "\(nightStayComossionNotesTextField.text ?? "0")",
            "total_paytodriver": "\(payToDriverTextField.text ?? "")",
            "total_sum_commision": "\(sumComission)",
            "total_sumof_commissions": "\(sumOfComission)",
            "code": 0,
            "create_by": "\(editDAta?.createby ?? name!)",
            "login_id": "\(id!)" ,
            "deletedAdvanceList":deletedAdvanceIDS ,"deletedExpenseList":deletedExpenseIds,"deletedJobList":deletedJobIds
          ]

        makePostCall(json: json, url: "\(constantsUsedInApp.baseUrl)taform/updateNewTaform")
        print(json)
    }
    func makePostCall(json: [String: Any] , url : String) {
        let decoder = JSONDecoder()
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(url)")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TaSaveJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                    
                            let alert = UIAlertController(title: "Ta Form", message: "\(loginBaseResponse?.response ?? "Saved")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                self.navigationController?.popToRootViewController(animated: true)
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Ta Form", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}

struct taformList {
    var clientID : String?
    var clientName : String?
    var commission : String?
    var jobDate : String?
    var jobFrom : String?
    var fromId : String?
    var jobNumber : String?
    var jobRemark : String?
    var jobTo : String?
    var toID : String?
    var truckNumber : String?
    var truckNumberID : String?
    var truckType : String?
    var truckTyeID  : String?
    var nightStay : nightStayData?
    var taDate : String?
    var taNumber : String?
    var driverDetails : DriversList
    
}

//
//  LocationListViewController.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-06.
//

import UIKit

class LocationListViewController: UIViewController, addLocationProtocol, UISearchBarDelegate {

    func refresh() {
        makeGetCall()
    }
    @IBOutlet var searchBarView: UISearchBar!
    
    @IBOutlet weak var searchBUtton: UIBarButtonItem!
    var searchMode = false
    var searchResult =  [LocationsList]()
    
    @IBOutlet weak var locationTableView: UITableView!
    var locationList : LocationListJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Locations"
        locationTableView.delegate = self
        locationTableView.dataSource = self
       
        makeGetCall()
        searchBarView.delegate = self

    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        search(searchTExt: searchText)
        
    }
    func search(searchTExt : String)
    {
        searchResult.removeAll()
        print(searchTExt)
        if locationList != nil
        {
            if let loc = locationList?.locationsList
            {
                for x in loc
                {
                    if x.location?.containsIgnoringCase(find: searchTExt) == true
                    {
                        searchResult.append(x)
                    }
                }
            }
        }
        locationTableView.reloadData()
        
    }

    @IBAction func searchButtonPressed(_ sender: Any) {
        
        if searchMode
        {
            searchBUtton.image = UIImage(systemName: "magnifyingglass")
            searchMode = false
            let textFieldInsideSearchBar = searchBarView.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.text = ""
            self.navigationItem.titleView = nil
            self.locationTableView.reloadData()
        }
        else
        {
            searchMode  = true
            searchBUtton.image = UIImage(systemName: "xmark")
        searchBarView.sizeToFit()
            let textFieldInsideSearchBar = searchBarView.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.becomeFirstResponder()

            textFieldInsideSearchBar?.textColor = .white
        self.navigationItem.titleView = searchBarView
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addLocation"
        {
            let vc = segue.destination as! AddLocation
            vc.delegate = self
        }
        if segue.identifier == "updateLocation"
        {
            let vc = segue.destination as! AddLocation
            vc.delegate = self
            let x = sender as! LocationsList
            vc.editData = x
        }
    }
    
}


extension LocationListViewController: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if searchMode
        {
             let x = searchResult[indexPath.row]
            
            cell.textLabel?.text = x.location?.uppercased() ?? ""
            cell.textLabel?.textColor = constantsUsedInApp.accentColor
            
        }
        else
        {
        if let x = locationList?.locationsList?[indexPath.row]
        {
          //  cell.clientAddress.text = x.clientDetails?.uppercased() ?? ""
            //cell.clientName.text = x.clientName?.uppercased() ?? ""
            cell.textLabel?.text = x.location?.uppercased() ?? ""
            cell.textLabel?.textColor = constantsUsedInApp.accentColor
        }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchMode
        {
            return searchResult.count
        }
        else
        {
        return locationList?.locationsList?.count ?? 0
        }
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        //  action.backgroundColor = .yellow
        let actionApply = UIContextualAction(style: .normal, title:  "Delete", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            success(true)
            let alert = UIAlertController(title: "Delete", message: "Are You Sure?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
                
                let x = locationList?.locationsList?[indexPath.row]
                let id = UserDefaults.standard.string(forKey: "loginID")

                let json : [String : Any] = ["location":"\(x?.location ?? "")","location_id":"\(x?.locationID ?? "")","code":0,"create_by":"\(x?.createdBy ?? "")","login_id":"\(id!)"]
                makePostDelete(json: json, url: "\(constantsUsedInApp.baseUrl)taform/deleteLocation", index: indexPath)

            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            
        })
        
        let actionEdit = UIContextualAction(style: .normal, title:  "Edit", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            success(true)
              self.locationTableView.reloadData()
            self.performSegue(withIdentifier: "updateLocation", sender: locationList?.locationsList?[indexPath.row])
            
        })
        
        actionApply.backgroundColor = .systemRed
        //  actionApply.image = UIImage(systemName: "hand.tap.fill")
        
        return UISwipeActionsConfiguration(actions: [actionApply , actionEdit])
    }
    
    func makeGetCall() {
        let decoder = JSONDecoder()
        
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllLocations")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(LocationListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            self.locationList = nil
                            print(loginBaseResponse as Any)
                            self.locationList = loginBaseResponse
                            self.locationTableView.reloadData()
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Location", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    func makePostDelete(json: [String: Any] , url : String , index : IndexPath) {
        let decoder = JSONDecoder()
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(url)")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TaSaveJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.makeGetCall()
                            let alert = UIAlertController(title: "Advance", message: "\(loginBaseResponse?.response ?? "Delete")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                print("delete")
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Advance", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}

//
//  MyJob.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-15.
//

import UIKit
import DatePickerDialog
class MyJob: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var bookingTableView: UITableView!
    var driverList : DriverListJSON?
    var textfieldPickers = [UITextField]()
    var selectedRow : IndexPath?
    var talist : TaListJSON?
    var selectedDriver = "All"
    var currentType = "Assigned"
    var fromDate = ""
    var toDate = ""
     var screenSize = UIScreen.main.bounds.size.height
     var screenWidth = UIScreen.main.bounds.size.width
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var driverTextField: UITextField!
    let expiryDatePicker = MonthYearPickerView()
    let toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 400, width: UIScreen.main.bounds.size.width, height: 50))
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title =  "Jobs List"
        bookingTableView.delegate = self
        bookingTableView.dataSource = self
        bookingTableView.register(UINib(nibName: reuseNibIdentifier.myJob, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.myJob)
        makeGetCall()
      //  dateLabel.isUserInteractionEnabled = true
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(monthYear))
        //dateLabel.addGestureRecognizer(gesture2)
        textfieldPickers = [driverTextField]
        for textField in 0 ... textfieldPickers.count - 1
        {
            textfieldPickers[textField].delegate = self
            createPickerView(textField: textfieldPickers[textField], tag: textField)
            //textfieldPickers[textField].font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        dismissPickerView()
       // makePostCallTaList()
        let dt = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "yyy-MM"
        let x = formatter2.string(from: dt)
        let calanderDate = Calendar.current.dateComponents([.day], from: dt)

        if calanderDate.day! > 25
        {
            fromDate = x
            let d = dt.adding(months: 1)
          //  d.addTimeInterval(60 / 60 / 24 / 30)
            print(formatter2.string(from: d!))
            toDate = formatter2.string(from: d!)
          //  makePostCallDelivered()
        //    dateLabel.isUserInteractionEnabled = true
            let gesture2 = UITapGestureRecognizer(target: self, action: #selector(monthYear))
          ///  dateLabel.addGestureRecognizer(gesture2)
           // dateLabel.text = "\(fromDate)-26 to \(toDate)-25"
        }
        else
        {
        toDate = x
        let d = dt.adding(months: -1)
      //  d.addTimeInterval(60 / 60 / 24 / 30)
        print(formatter2.string(from: d!))
        fromDate = formatter2.string(from: d!)
        //makePostCallDelivered()
      //  dateLabel.isUserInteractionEnabled = true
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(monthYear))
        //dateLabel.addGestureRecognizer(gesture2)
        //dateLabel.text = "\(fromDate)-26 to \(toDate)-25"
        
        }

   
        makePostCallTaList()

    }
    @objc func monthYear()
    {
        let month = Calendar.current.component(.month, from: Date())
        _ = Calendar.current.component(.year, from: Date())
        var m = "\(month)"
        if m.count == 1
        {
            print("1")
            m = "0\(month)"
        }
       // historyDate = "\(year)-\(m)"
        expiryDatePicker.backgroundColor = UIColor.white
           expiryDatePicker.setValue(UIColor.black, forKey: "textColor")
           expiryDatePicker.autoresizingMask = .flexibleWidth
           expiryDatePicker.contentMode = .center
        expiryDatePicker.frame = CGRect.init(x: 0.0, y: self.screenSize - 400, width: UIScreen.main.bounds.size.width, height: 400)
           self.view.addSubview(expiryDatePicker)

           toolBar.barStyle = .default
           toolBar.isTranslucent = true
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.actionPicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)

           toolBar.setItems([spaceButton, doneButton], animated: false)
           toolBar.isUserInteractionEnabled = true
           self.view.addSubview(toolBar)

           //DISABLE RIGHT ITEM & LEFT ITEM
          //        disableCancelAndSaveItems()

           //DISABLED SELECTION FOR ALL CELLS
          //        ableToSelectCellsAndButtons(isAble: false)

           //DISABLE RIGHT ITEM & LEFT ITEM
          // isEnableCancelAndSaveItems(isEnabled: false)

           //SHOW GREY BACK GROUND
          // showGreyOutView(isShow: true)
    
        expiryDatePicker.onDateSelected = { [self] (month: Int, year: Int) in
              // self.expiredDetailOutlet.text = string
            var m = "\(month)"
            if m.count == 1
            {
                print(month , year)
                m = "0\(month)"
            }

            //let formatter = DateFormatter()
            //formatter.dateFormat = "dd-MM-yyyy"
            let formatter2 = DateFormatter()
            formatter2.dateFormat = "yyyy-MM"
            let z = formatter2.date(from: "\(year)-\(m)")
            let x = formatter2.string(from: z!)
            toDate = x
            let d = z!.adding(months: -1)
          //  d.addTimeInterval(60 / 60 / 24 / 30)
            print(formatter2.string(from: d!))
            fromDate = formatter2.string(from: d!)
       //     dateLabel.text = "\(fromDate)-26 to \(toDate)-25"

          //  self.historyDate = "\(year)-\(m)"

               
           }
        
    }
    @objc func actionPicker() {
        expiryDatePicker.removeFromSuperview()
        toolBar.removeFromSuperview()
        view.endEditing(true)
        makePostCallTaList()

        
    }
    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in textfieldPickers
        {
            textField.inputAccessoryView = toolBar
        }
        
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    
    @IBAction func bookingType(_ sender: Any) {
        let x = sender as! UISegmentedControl
        
        print(x.selectedSegmentIndex)
        if x.selectedSegmentIndex == 0
        {
            currentType = "Assigned"
        }
        else
        {
            currentType = "Pending"
        }
        makePostCallTaList()
    }
    @objc func showDateOrTime(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UILabel
        switch x {
        case dateLabel :
        datePickerTapped(sender: x!)
            print("date")

        default:
            print("error in time date")
        }
        
    }
    func datePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { [self] (date) in
            if let dt = date {
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                let formatter2 = DateFormatter()
                formatter2.dateFormat = "yyy-MM"
                let x = formatter2.string(from: dt)
                toDate = x
                let d = dt.adding(months: -1)
              //  d.addTimeInterval(60 / 60 / 24 / 30)
                print(formatter2.string(from: d!))
                fromDate = formatter2.string(from: d!)
                makePostCallTaList()
                sender.text = "\(fromDate)-26 to \(toDate)-25"
            }
        }
        
    }

    func makeGetCall() {
        let decoder = JSONDecoder()
      
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllDrivers")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                        print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(DriverListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                         //   print(loginBaseResponse as Any)
                            self.driverList = loginBaseResponse
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    func makePostCallTaList() {
        let decoder = JSONDecoder()

        let json: [String: Any] = [ "driver_id":"\(selectedDriver)","enddate":"","startdate":"","ta_status":"\(currentType)"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getTaformJobsList")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                  //  print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TaListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        self.talist = nil
                        self.bookingTableView.reloadData()
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            //print(loginBaseResponse as Any)
                            self.selectedRow = nil
                            self.talist = nil
                            self.talist = loginBaseResponse
                            self.bookingTableView.reloadData()
                            
                            print(loginBaseResponse?.taformList?[0].jobTaformList?.count as Any)

                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "My Bookings", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

 

}

extension MyJob: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = bookingTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.myJob) as! MyJobCell
        if let x = talist?.taformList?[indexPath.row]
        {
            cell.date.text = convertDateFormater(x.taFormdate ?? "")
            cell.taNumber.text = x.taTadynamicNumber
            cell.comission.text = x.totalCommision
            cell.advance.text = x.totalAdvance
            cell.payToDriver.text = x.totalPaytodriver
            cell.name.text = x.taDriverName
            cell.createBy.text = x.createby
            cell.jobDetails = x.jobTaformList
            // manage account opetatin
            //Pending  Verified
            if x.managerStatus == "Pending"
            {
                cell.manage.image = UIImage(named: "clock")?.withTintColor(.systemRed)
            }
            else
            {
                cell.manage.image = UIImage(named: "checked")?.withTintColor(.systemGreen)
            }
            if x.accountStatus == "Pending"
            {
                cell.account.image = UIImage(named: "clock")?.withTintColor(.systemRed)
            }
            else
            {
                cell.account.image = UIImage(named: "checked")?.withTintColor(.systemGreen)
            }

            if x.operationStatus == "Pending"
            {
                cell.operation.image = UIImage(named: "clock")?.withTintColor(.systemRed)
            }
            else
            {
                cell.operation.image = UIImage(named: "checked")?.withTintColor(.systemGreen)
            }

            
            
            
        }
                return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath == selectedRow
        {
            let x = 140
            let c = talist?.taformList?[indexPath.row].jobTaformList?.count ?? 0
            let h = (c * 100) + x
            print("height \(indexPath.row)     \(h)")
            print(c)
            return CGFloat(h)    //140 // 240
        }
        else
        {
        return 44
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return talist?.taformList?.count ?? 0
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
         {
       
            //  action.backgroundColor = .yellow
            let actionApply = UIContextualAction(style: .normal, title:  "Update", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                      print("Apply")
                  success(true)
                performSegue(withIdentifier: "updateTA", sender: indexPath.row)
                                  
                  })
       

          actionApply.backgroundColor = .green
        //  actionApply.image = UIImage(systemName: "hand.tap.fill")

                  return UISwipeActionsConfiguration(actions: [actionApply])
            
        
      

     
         }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updateTA"
        {
            let i = sender as! Int
            let vc = segue.destination as! NewBooking
            vc.newFormWithoutAdvanceExpense = true
            vc.updateTaForm = talist?.taformList?[i]
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        bookingTableView.deselectRow(at: indexPath, animated: true)
        selectedRow = indexPath
        bookingTableView.reloadData()
       // print(talist)
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }
}

extension MyJob : UIPickerViewDelegate , UIPickerViewDataSource
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            textfieldPickers[0].text = driverList?.driversList?[0].driverName ?? ""

        default:
            print("err")
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return driverList?.driversList?.count ?? 0
        case 1:
            return 3
        case 2:
            return 2
        case 3:
            return 3
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return driverList?.driversList?[row].driverName ?? ""
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            textfieldPickers[0].text = driverList?.driversList?[row].driverName ?? ""
            selectedDriver = driverList?.driversList?[row].driverIDPrimary ?? ""
            self.view.endEditing(true)

            makePostCallTaList()
        default:
            print("err")
            print(pickerView.tag)
        }
    }
}



//
//  Dashboard.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-01.
//

import UIKit
import ImageSlideshow
import Alamofire
var announcementImages = [UIImage]()

class Dashboard: UIViewController, ImageSlideshowDelegate {
    @IBOutlet weak var dashboardCollection: UICollectionView!
    @IBOutlet weak var viewContainingDashboard: UIView!
    @IBOutlet weak var announcementCollectionView: UICollectionView!
    @IBOutlet weak var slideshow: ImageSlideshow!
    var announcementImages = [UIImage]()

    @IBOutlet weak var dashboardHEight: NSLayoutConstraint!
    @IBOutlet weak var mybookingView: UIView!
    @IBOutlet weak var bookingView: UIView!
    var imageUrl = ["https://glowfreight.com.my/images/tq_new_1.png" , "https://glowfreight.com.my/images/about_us.png" , "https://glowfreight.com.my/images/trade_equipment.png" , "https://glowfreight.com.my/images/home_gallery1.png"]
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
      
        self.navigationController?.navigationBar.isTranslucent = false
   
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
      //  self.navigationController?.isNavigationBarHidden = true

    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        let name = UserDefaults.standard.string(forKey: "name")
        self.title = name
        
        dashboardCollection.register(UINib(nibName: reuseNibIdentifier.dashboard, bundle: nil), forCellWithReuseIdentifier: reuseCellIdentifier.dashboard)
        dashboardCollection.delegate = self
        dashboardCollection.dataSource = self
        slideshow.delegate = self
        slideshow.slideshowInterval = 4

        for n in imageUrl
        {
                let urlStr = n
                
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async { [self] in
                        self.announcementImages.append(UIImage(data: data!)!)

                        self.reloadAnnouncementImage()
                
                    }
                }
            
        }
        
    }
    func reloadAnnouncementImage()
    {
        var imageSource = [ImageSource]()
        for n in 0 ... announcementImages.count - 1
        {
            let x =  ImageSource(image: announcementImages[n])
            imageSource.append(x)
        }
        slideshow.setImageInputs(imageSource)
        slideshow.contentScaleMode = .scaleToFill

    }
    @objc func toNewBooking()
    {
        performSegue(withIdentifier: "dashToNew", sender: nil)
    }

    @objc func toMyBooking()
    {
        performSegue(withIdentifier: "dashToMY", sender: nil)
    }
    @IBAction func logOutpressed(_ sender: Any) {
        UserDefaults.standard.setValue("FALSE", forKey: "loggedIn")
        let storyboard2 = UIStoryboard(name: "Login", bundle: nil)

        let dashboard = storyboard2.instantiateViewController(identifier: "login")

        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard)
    }
    

}

extension Dashboard : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == announcementCollectionView
        {
            return 3
        }
        else
        {
            
            let role = UserDefaults.standard.string(forKey: "loginrole")
            if role! == "operation"
            {
                return 3

            }
            else
            {
                  dashboardHEight.constant = 360

                                return 9
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == announcementCollectionView
        {
            let cell = announcementCollectionView.cellForItem(at: indexPath) as! DashboardCell
            return cell
        }
        else
        {
            let cell = dashboardCollection.dequeueReusableCell(withReuseIdentifier: reuseCellIdentifier.dashboard, for: indexPath) as! DashboardCell
            let c = constantsUsedInApp.accentColor
            cell.mainView.layer.cornerRadius = 5
            cell.mainView.layer.borderWidth = 3
            cell.mainView.layer.borderColor = c.cgColor
            switch indexPath.row {
            case 0:
                cell.title.text = "New Booking"
                cell.image.image = UIImage(named: "my_bookings")?.withTintColor(c)
            case 1:
                cell.title.text = "My Bookings"
                cell.image.image = UIImage(named: "bookings")?.withTintColor(c)
            case 2:
                cell.title.text = "Drivers"
                cell.image.image = UIImage(named: "Driver")?.withTintColor(c)
            case 3:
                cell.title.text = "Clients"
                cell.image.image = UIImage(named: "client_list")?.withTintColor(c)

            case 4:
                cell.title.text = "Advance"
                cell.image.image = UIImage(named: "payment_types_list")?.withTintColor(c)
            case 5:
                cell.title.text = "Location"
                cell.image.image = UIImage(named: "location_list")?.withTintColor(c)
            case 6:
                cell.title.text = "Truck Type"
                cell.image.image = UIImage(named: "truck_list")?.withTintColor(c)
            case 7:
                cell.title.text = "Truck Number"
                cell.image.image = UIImage(named: "truck_number_list")?.withTintColor(c)
            case 8:
                cell.title.text = "Report"
                cell.image.image = UIImage(named: "baseline_insert_chart_outlined_black_48dp")?.withTintColor(c)
                
            default:
                print("err")
            }
            return cell
        }

        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == announcementCollectionView
        {
        
            return CGSize(width: view.frame.width, height: announcementCollectionView.frame.height)

        
        }
        else
        {
          
           // let h = dashboardCollection.frame.height
            return CGSize(width: view.frame.width / 3.34, height: 110)
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "dashToNew"
        {
            let vc = segue.destination as! NewBooking
            vc.newFormWithoutAdvanceExpense = true
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == dashboardCollection
        {
            
            switch indexPath.row {
            case 0:
                performSegue(withIdentifier: "dashToNew", sender: nil)
                
            case 1:
                
                performSegue(withIdentifier: "dashToMY", sender: nil)

            case 2:
                
                performSegue(withIdentifier: "dashToDriv", sender: nil)
            case 3:
                
                performSegue(withIdentifier: "dashToclient", sender: nil)
            case 4:
                
                performSegue(withIdentifier: "dashToAdvacne", sender: nil)
            case 5:
                
                performSegue(withIdentifier: "dashToLocation", sender: nil)
            case 6:
                
                performSegue(withIdentifier: "dashToTruck", sender: nil)
            case 7:
                
                performSegue(withIdentifier: "dashToTruckNumber", sender: nil)
            case 8:
                
                performSegue(withIdentifier: "dashtoreport", sender: nil)
                
                
                
            default:
                print("err")
            }
        }
    }
}

extension CALayer {

func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {

    let border = CALayer()

    switch edge {
    case .top:
        border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
    case .bottom:
        border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
    case .left:
        let h = frame.height / 4
        border.frame = CGRect(x: 0, y: h / 2, width: thickness, height: frame.height - h)
    case .right:
        border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
    default:
        break
    }
    border.backgroundColor = color.cgColor
    addSublayer(border)
    }
}

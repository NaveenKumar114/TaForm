//
//  ClientList.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-04.
//

import UIKit

class ClientList: UIViewController, addClientProtocol, UISearchBarDelegate {
    func refresh() {
        makeGetCall()
    }
 
    
    @IBOutlet var searchBarView: UISearchBar!
    
    @IBOutlet weak var searchBUtton: UIBarButtonItem!
    @IBOutlet weak var clientTableView: UITableView!
    var searchMode = false
    var clientList : ClientListJSON?
    var searchResult =  [ClientsList]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Clients"
        clientTableView.delegate = self
        clientTableView.dataSource = self
        clientTableView.register(UINib(nibName: reuseNibIdentifier.clientList, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.clientList)
        makeGetCall()
        searchBarView.delegate = self
        
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        
        if searchMode
        {
            searchBUtton.image = UIImage(systemName: "magnifyingglass")
            searchMode = false
            let textFieldInsideSearchBar = searchBarView.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.text = ""
            self.navigationItem.titleView = nil
            self.clientTableView.reloadData()
        }
        else
        {
            searchMode  = true
            searchBUtton.image = UIImage(systemName: "xmark")
        searchBarView.sizeToFit()
            let textFieldInsideSearchBar = searchBarView.value(forKey: "searchField") as? UITextField
            textFieldInsideSearchBar?.becomeFirstResponder()

            textFieldInsideSearchBar?.textColor = .white
        self.navigationItem.titleView = searchBarView
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addclient"
        {
            let vc = segue.destination as! AddClient
            vc.delegate = self
        }
        if segue.identifier == "updateClient"
        {
            let vc = segue.destination as! AddClient
            vc.delegate = self
            let x = sender as! ClientsList
            vc.editData = x
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        search(searchTExt: searchText)
        
    }
    func search(searchTExt : String)
    {
        searchResult.removeAll()
        print(searchTExt)
        if clientList != nil
        {
            if let client = clientList?.clientsList
            {
                for x in client
                {
                    if x.clientName?.containsIgnoringCase(find: searchTExt) == true
                    {
                        searchResult.append(x)
                    }
                }
            }
        }
        clientTableView.reloadData()
        
    }
}


extension ClientList: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = clientTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.clientList) as! ClientListCell
        if searchMode
        {
             let x = searchResult[indexPath.row]
            
                cell.clientAddress.text = x.clientDetails?.uppercased() ?? ""
                cell.clientName.text = x.clientName?.uppercased() ?? ""
            
        }
        else
        {
        if let x = clientList?.clientsList?[indexPath.row]
        {
            cell.clientAddress.text = x.clientDetails?.uppercased() ?? ""
            cell.clientName.text = x.clientName?.uppercased() ?? ""
        }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchMode
        {
            print("gg")
            return searchResult.count
        }
        else
        {
        return clientList?.clientsList?.count ?? 0
        }
        
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        
        //  action.backgroundColor = .yellow
        let actionApply = UIContextualAction(style: .normal, title:  "Delete", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            success(true)
            let alert = UIAlertController(title: "Delete", message: "Are You Sure?", preferredStyle: .alert)
            
            let ok = UIAlertAction(title: "Yes", style: .default, handler: { action in
                
                let x = clientList?.clientsList?[indexPath.row]
                let json : [String : Any] = ["client_details":"\(x?.clientDetails ?? "")","client_id":"\(x?.clientID ?? "0")","client_name":"\(x?.clientName ?? "")","code":0,"created_by":"\(x?.createdBy ?? "")","created_date":"\(x?.createdDate ?? "")"]
                makePostDelete(json: json, url: "\(constantsUsedInApp.baseUrl)taform/deleteClient", index: indexPath)

            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "No", style: .default, handler: { action in
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            
        })
        
        let actionEdit = UIContextualAction(style: .normal, title:  "Edit", handler: { [self] (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            success(true)
            //  self.clientTableView.reloadData()
            self.performSegue(withIdentifier: "updateClient", sender: clientList?.clientsList?[indexPath.row])
            
        })
        
        actionApply.backgroundColor = .systemRed
        //  actionApply.image = UIImage(systemName: "hand.tap.fill")
        
        return UISwipeActionsConfiguration(actions: [actionApply , actionEdit])
    }
    
    func makeGetCall() {
        let decoder = JSONDecoder()
        
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/getAllClients")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(ClientListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            self.clientList = nil
                            print(loginBaseResponse as Any)
                            self.clientList = loginBaseResponse
                            self.clientTableView.reloadData()
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Drivers", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    func makePostDelete(json: [String: Any] , url : String , index : IndexPath) {
        let decoder = JSONDecoder()
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(url)")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TaSaveJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.makeGetCall()
                            let alert = UIAlertController(title: "Client", message: "\(loginBaseResponse?.response ?? "Delete")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                print("delete")
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Client", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}

//
//  AddLocation.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-07.
//

import UIKit

class AddLocation: UIViewController, UITextFieldDelegate {

   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var locationName: UITextField!
    
    var delegate : addLocationProtocol?
    var editData : LocationsList?
    override func viewDidLoad() {
        super.viewDidLoad()
        locationName.delegate = self
        if let x = editData
        {
            locationName.text = x.location ?? ""
            self.title = "Update Location"
        }
    }
    

    @IBAction func submitButton(_ sender: Any) {
        if locationName.text == ""
        {
            let alert = UIAlertController(title: "Location", message: "Please enter all data", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if editData != nil
            {
                
             //   let roletype = UserDefaults.standard.string(forKey: "loginrole")
                let id = UserDefaults.standard.string(forKey: "loginID")
                let json : [String : Any] = ["location":"\(locationName.text!)","location_id":"\(editData?.locationID ?? "")","code":0,"create_by":"\(editData?.createdBy ?? "")","login_id":"\(id!)"]
                print(json)
                makePostCall(json: json, url: "\(constantsUsedInApp.baseUrl)taform/save_updateLocation")
            }
            else
            {
            let name = UserDefaults.standard.string(forKey: "name")
            
         //   let roletype = UserDefaults.standard.string(forKey: "loginrole")
            let id = UserDefaults.standard.string(forKey: "loginID")
            let json : [String : Any] = ["location":"\(locationName.text!)","location_id":"0","code":0,"create_by":"\(name!)","login_id":"\(id!)"]
            makePostCall(json: json, url: "\(constantsUsedInApp.baseUrl)taform/save_updateLocation")
            }
        }
    }
    @IBAction func cancelButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func makePostCall(json: [String: Any] , url : String) {
        let decoder = JSONDecoder()
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(url)")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(TaSaveJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                    
                            let alert = UIAlertController(title: "Location", message: "\(loginBaseResponse?.response ?? "Saved")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                self.delegate?.refresh()
                                self.navigationController?.popViewController(animated: true)
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Location", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

}

protocol addLocationProtocol {
    func refresh()
}

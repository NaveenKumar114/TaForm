//
//  Login.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-01.
//

import UIKit
import Messages
import Firebase
import FirebaseAuth
class Login: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var loginText: UILabel!
    @IBOutlet weak var roleButton: UIButton!
    var tokenID = ""

    var role = "operation"
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
        passwordTextField.delegate = self
       // emailTextField.text = "muhun@glowfreight.com.my"
        emailTextField.text = "raj@glowfreight.com.my"

        passwordTextField.text = "123456"
        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
           
            self.tokenID = token
          //  self.fcmRegTokenMessage.text  = "Remote FCM registration token: \(token)"
          }
        }

        // Do any additional setup after loading the view.
    }
    @IBAction func roleButtonPressed(_ sender: Any) {
        if role == "operation"
        {
            roleButton.setTitle("Operation Login?", for: .normal)
            loginText.text = "Admin Login"
            role = "admin"
        }
        else
        {
            roleButton.setTitle("Admin Login?", for: .normal)
            loginText.text = "Operation Login"
            role = "operation"
        }
        print(role)
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        if emailTextField.text == "" || passwordTextField.text == ""
        {
            let alert = UIAlertController(title: "Login", message: "Please Enter All Details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            makePostCall(email: emailTextField.text!, password: passwordTextField.text!)
        }
    }
    func makePostCall(email : String , password : String) {
        let decoder = JSONDecoder()
        let json: [String: Any] = ["email": "\(email)",
                                   "password": "\(password)" , "tokenid":"\(tokenID)" , "roletype": "\(role)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(constantsUsedInApp.baseUrl)taform/ios_login_post")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            print(url)
            print(json)
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            if loginBaseResponse?.logindetails?.roleType ?? "" == role
                            {
                                
                            
                            print(loginBaseResponse as Any)
                            UserDefaults.standard.setValue("\(loginBaseResponse!.logindetails!.userName!)", forKey: "name")
                            UserDefaults.standard.setValue("\(loginBaseResponse!.logindetails!.userID!)", forKey: "loginID")
                            UserDefaults.standard.setValue("\(loginBaseResponse!.logindetails!.roleType!)", forKey: "loginrole")

                            UserDefaults.standard.setValue("TRUE", forKey: "loggedIn")
                                let storyboard3 = UIStoryboard(name: "Dashboard", bundle: nil)

                                let dashboard3 = storyboard3.instantiateViewController(identifier: "dashboa")
                                        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard3)

                            }
                            else
                            {
                                let alert = UIAlertController(title: "Login", message: "Invalid Role Type", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)

                            }


                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Login", message: "\(loginBaseResponse?.response ?? "error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

}

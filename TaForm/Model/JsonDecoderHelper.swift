//
//  JsonDecoderHelper.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-03.
//

import Foundation
import UIKit
class jsonDecodeHelper
{
    
    func makePostCall(json: [String: Any] , urlString : String) {
        let decoder = JSONDecoder()
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string: urlString)
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(LoginJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            alert.presentInOwnWindow(animated: true, completion: {
                                print("completed")
                            })
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

}


extension UIAlertController {

    func presentInOwnWindow(animated: Bool, completion: (() -> Void)?) {
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(self, animated: animated, completion: completion)
    }

}

//
//  DeleteDriverJson.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-15.
//

import Foundation

// MARK: - DeleteDriverJSON
struct DeleteDriverJSON: Codable {
    let code: Int?
    let response: String?
}

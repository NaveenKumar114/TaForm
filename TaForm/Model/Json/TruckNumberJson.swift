//
//  TruckNumberJson.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-05.
//

import Foundation

// MARK: - TruckNumberJSON
struct TruckNumberJSON: Codable {
    let code: Int?
    let response: String?
    let truckNumberList: [TruckNumberList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case truckNumberList = "truck_number_list"
    }
}

// MARK: - TruckNumberList
struct TruckNumberList: Codable {
    let truckNumberID, truckNo, truckType, createdID: String?
    let createBy, createDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case truckNumberID = "truck_number_id"
        case truckNo = "truck_no"
        case truckType = "truck_type"
        case createdID = "created_id"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}

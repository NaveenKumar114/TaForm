//
//  DriverListJson.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-03.
//

import Foundation

// MARK: - DriverListJSON
struct DriverListJSON: Codable {
    let code: Int?
    let response, tanumber: String?
    var driversList: [DriversList]?

    enum CodingKeys: String, CodingKey {
        case code, response, tanumber
        case driversList = "drivers_list"
    }
}

// MARK: - DriversList
struct DriversList: Codable {
    let driverIDPrimary, vendorID: String?
    let joinDate: String?
    let vendorOptiondriver, categoryDriver, driverName, userID: String?
    let createdBy, driverJobNumber, driverJobID, driverTruckID: String?
    let subscriberID, driverEmail: String?
    let driverPassword, driverMobile, driverAddress: String?
    let driverNrc, driverNrcFile: String?
    let profileImage, driverLicense, driverLicenseFile, driverLicenseExpiry: String?
    let isActive, createdOn: String?
    let driversListCreatedBy: String?
    let updatedOn: String?
    let updatedBy: String?
    let tokenid, jobStatus, currentposition, latitude: String?
    let longitude, speed, bearing, batteryType: String?
    let batteryPercentage, networkType, networkOperator, phoneType: String?
    let deviceName, deviceManufracture, deviceBrand, deviceOS: String?
    let deviceModel, geofence: String?

    enum CodingKeys: String, CodingKey {
        case driverIDPrimary = "driver_id_primary"
        case vendorID = "vendor_id"
        case joinDate = "join_date"
        case vendorOptiondriver = "vendor_optiondriver"
        case categoryDriver = "category_driver"
        case driverName = "driver_name"
        case userID = "user_id"
        case createdBy
        case driverJobNumber = "driver_job_number"
        case driverJobID = "driver_job_id"
        case driverTruckID = "driver_truck_id"
        case subscriberID = "subscriber_id"
        case driverEmail = "driver_email"
        case driverPassword = "driver_password"
        case driverMobile = "driver_mobile"
        case driverAddress = "driver_address"
        case driverNrc = "driver_nrc"
        case driverNrcFile = "driver_nrc_file"
        case profileImage = "profile_image"
        case driverLicense = "driver_license"
        case driverLicenseFile = "driver_license_file"
        case driverLicenseExpiry = "driver_license_expiry"
        case isActive = "is_active"
        case createdOn = "created_on"
        case driversListCreatedBy = "created_by"
        case updatedOn = "updated_on"
        case updatedBy = "updated_by"
        case tokenid
        case jobStatus = "job_status"
        case currentposition, latitude, longitude, speed, bearing
        case batteryType = "battery_type"
        case batteryPercentage = "battery_percentage"
        case networkType = "network_type"
        case networkOperator = "network_operator"
        case phoneType = "phone_type"
        case deviceName = "device_name"
        case deviceManufracture = "device_manufracture"
        case deviceBrand = "device_brand"
        case deviceOS = "device_os"
        case deviceModel = "device_model"
        case geofence
    }
}

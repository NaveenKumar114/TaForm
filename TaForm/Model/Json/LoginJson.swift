//
//  LoginJson.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-03.
//


import Foundation

// MARK: - LoginJSON
struct LoginJSON: Codable {
    let code: Int?
    let response: String?
    let logindetails: Logindetails?
}

// MARK: - Logindetails
struct Logindetails: Codable {
    let userID, email, password, userName: String?
    let roleType, profilePic, createdBy, createdID: String?
    let createdDtm, updatedBy, updatedDtm, tokenid: String?
    let profileImage: String?

    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case email, password
        case userName = "user_name"
        case roleType = "role_type"
        case profilePic = "profile_pic"
        case createdBy
        case createdID = "createdId"
        case createdDtm, updatedBy, updatedDtm, tokenid
        case profileImage = "profile_image"
    }
}

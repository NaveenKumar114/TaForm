//
//  ReportChartJSON.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-10.
//

import Foundation

struct ReportChartJSON: Codable {
    let code: Int?
    let response: String?
    let chart: [Chart]?
    let expense: [Expense]?
    let chartSevan, chartall: [Chart]?
    let chartExpenseMonthly, chartExpenseSevan, chartExpenseAll: [ChartExpense]?
    let chartExpenseRequestSevan, chartExpenseRequestMonthly, chartExpenseRequestYear: [ChartExpenseRequest]?
    let chartExpenseTypeLastmonth, chartExpenseTypeMonthly, chartExpenseTypeYear: [ChartExpenseType]?
    let chartManagerLastmonth, chartManagerMonthly, chartManagerYear, chartAccountLastmonth: [ChartAccountLastmonthElement]?
    let chartAccountMonthly, chartAccountYear: [ChartAccountLastmonthElement]?

    enum CodingKeys: String, CodingKey {
        case code, response, chart, expense
        case chartSevan = "chart_sevan"
        case chartall
        case chartExpenseMonthly = "chart_expense_monthly"
        case chartExpenseSevan = "chart_expense_sevan"
        case chartExpenseAll = "chart_expense_all"
        case chartExpenseRequestSevan = "chart_expense_request_sevan"
        case chartExpenseRequestMonthly = "chart_expense_request_monthly"
        case chartExpenseRequestYear = "chart_expense_request_year"
        case chartExpenseTypeLastmonth = "chart_expense_type_lastmonth"
        case chartExpenseTypeMonthly = "chart_expense_type_monthly"
        case chartExpenseTypeYear = "chart_expense_type_year"
        case chartManagerLastmonth = "chart_manager_lastmonth"
        case chartManagerMonthly = "chart_manager_monthly"
        case chartManagerYear = "chart_manager_year"
        case chartAccountLastmonth = "chart_account_lastmonth"
        case chartAccountMonthly = "chart_account_monthly"
        case chartAccountYear = "chart_account_year"
    }
}

// MARK: - Chart
struct Chart: Codable {
    let taDriverName, sumCommision: String?

    enum CodingKeys: String, CodingKey {
        case taDriverName = "ta_driver_name"
        case sumCommision = "sum_commision"
    }
}

// MARK: - ChartAccountLastmonthElement
struct ChartAccountLastmonthElement: Codable {
    let pending, verified: String?
}

// MARK: - ChartExpense
struct ChartExpense: Codable {
    let taDriverName, totalExpense, sumCommision: String?

    enum CodingKeys: String, CodingKey {
        case taDriverName = "ta_driver_name"
        case totalExpense = "Total_Expense"
        case sumCommision = "sum_commision"
    }
}

// MARK: - ChartExpenseRequest
struct ChartExpenseRequest: Codable {
    let truckTypeName, count: String?

    enum CodingKeys: String, CodingKey {
        case truckTypeName = "truck_type_name"
        case count
    }
}

// MARK: - ChartExpenseType
struct ChartExpenseType: Codable {
    let expenseType, totalExpense: String?

    enum CodingKeys: String, CodingKey {
        case expenseType = "expense_type"
        case totalExpense = "total_expense"
    }
}

// MARK: - Expense
struct Expense: Codable {
    let taDriverName, totalExpense: String?

    enum CodingKeys: String, CodingKey {
        case taDriverName = "ta_driver_name"
        case totalExpense = "Total_Expense"
    }
}

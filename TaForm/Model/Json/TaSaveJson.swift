//
//  TaSaveJson.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-08.
//

import Foundation

// MARK: - TaSaveJSON
struct TaSaveJSON: Codable {
    let code: Int?
    let response: String?
}

//
//  AdvanceTypeJson.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-06.
//

import Foundation

// MARK: - AdvanceTypeJSON
struct AdvanceTypeJSON: Codable {
    let code: Int?
    let response: String?
    let advanceList: [AdvanceList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case advanceList = "advance_list"
    }
}

// MARK: - AdvanceList
struct AdvanceList: Codable {
    let advanceID, advanceName, createdDate: String?
    let createdBy: String?
    let createdID, updatedDate, updatedBy: String?

    enum CodingKeys: String, CodingKey {
        case advanceID = "advance_id"
        case advanceName = "advance_name"
        case createdDate = "created_date"
        case createdBy = "created_by"
        case createdID = "created_id"
        case updatedDate = "updated_date"
        case updatedBy = "updated_by"
    }
}


//
//  LocationListJson.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-05.
//

import Foundation

// MARK: - LocationListJSON
struct LocationListJSON: Codable {
    let code: Int?
    let response: String?
    let locationsList: [LocationsList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case locationsList = "locations_list"
    }
}

// MARK: - LocationsList
struct LocationsList: Codable {
    let locationID, location, createdBy, createdID: String?
    let createdDate, updatedBy, updatedDate: String?

    enum CodingKeys: String, CodingKey {
        case locationID = "location_id"
        case location
        case createdBy = "created_by"
        case createdID = "created_id"
        case createdDate = "created_date"
        case updatedBy = "updated_by"
        case updatedDate = "updated_date"
    }
}

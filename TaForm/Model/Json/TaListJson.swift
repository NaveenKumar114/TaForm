//
//  TaListJson.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-12.
//

import Foundation

// MARK: - TaListJSON
struct TaListJSON: Codable {
    let code: Int?
    let response: String?
    let taformList: [TaformList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case taformList = "taform_list"
    }
}

// MARK: - TaformList
struct TaformList: Codable {
    let taFormID, sequence, taStatus, totalCommision: String?
    let totalAdvance, totalBalance, totalGivenbydriver, totalPaytodriver: String?
    let totalCashSpend, sumOfJobCommision, sumOfCashExpense, sumOfCardExpense: String?
    let totalJobCommission, nightDutyCommision, addtionalPickupCommsion, totalJobCommissionRemark: String?
    let nightDutyCommisionRemark, addtionalPickupCommsionRemark, taDriverName, taDriverID: String?
    let taTadynamicNumber, taFormdate, operationStatus, accountStatus: String?
    let managerStatus, managerStatusBy: String?
    let managerStatusTime: String?
    let operationStatusBy, operationStatusTime, accountStatusBy: String?
    let accountStatusTime: String?
    let operationDuration, accountDuration, managerDuration, rejectedTaID: String?
    let rejectedTaNumber, rejectedComment, rejectFlag, requestComment: String?
    let cancelComment, rejectedBy, rejectLoginID: String?
    let requestType: String?
    let isOpenFlag, openBy, openByID, createddate: String?
    let modifydate, createby, modifyby, loginID: String?
    let jobTaformList: [JobTaformList]?
    let advanceList: [AdvanceListHistory]?
    let expensesList: [ExpensesList]?

    enum CodingKeys: String, CodingKey {
        case taFormID = "ta_form_id"
        case sequence
        case taStatus = "ta_status"
        case totalCommision = "total_commision"
        case totalAdvance = "total_advance"
        case totalBalance = "total_balance"
        case totalGivenbydriver = "total_givenbydriver"
        case totalPaytodriver = "total_paytodriver"
        case totalCashSpend = "total_cash_spend"
        case sumOfJobCommision = "sum_of_job_commision"
        case sumOfCashExpense = "sum_of_cash_expense"
        case sumOfCardExpense = "sum_of_card_expense"
        case totalJobCommission = "total_job_commission"
        case nightDutyCommision = "night_duty_commision"
        case addtionalPickupCommsion = "addtional_pickup_commsion"
        case totalJobCommissionRemark = "total_job_commission_remark"
        case nightDutyCommisionRemark = "night_duty_commision_remark"
        case addtionalPickupCommsionRemark = "addtional_pickup_commsion_remark"
        case taDriverName = "ta_driver_name"
        case taDriverID = "ta_driver_id"
        case taTadynamicNumber = "ta_tadynamic_number"
        case taFormdate = "ta_formdate"
        case operationStatus = "operation_status"
        case accountStatus = "account_status"
        case managerStatus = "manager_status"
        case managerStatusBy = "manager_status_by"
        case managerStatusTime = "manager_status_time"
        case operationStatusBy = "operation_status_by"
        case operationStatusTime = "operation_status_time"
        case accountStatusBy = "account_status_by"
        case accountStatusTime = "account_status_time"
        case operationDuration = "operation_duration"
        case accountDuration = "account_duration"
        case managerDuration = "manager_duration"
        case rejectedTaID = "rejected_ta_id"
        case rejectedTaNumber = "rejected_ta_number"
        case rejectedComment = "rejected_comment"
        case rejectFlag = "reject_flag"
        case requestComment = "request_comment"
        case cancelComment = "cancel_comment"
        case rejectedBy = "rejected_by"
        case rejectLoginID = "reject_login_id"
        case requestType = "request_type"
        case isOpenFlag = "is_open_flag"
        case openBy = "open_by"
        case openByID = "open_by_id"
        case createddate, modifydate, createby, modifyby
        case loginID = "login_id"
        case jobTaformList, advanceList, expensesList
    }
}

// MARK: - AdvanceList
struct AdvanceListHistory: Codable {
    let advanceID, taFormID, loginID, advanceDate: String?
    let advanceType, advanceAmount, advanceCreateBy, advanceModifyBy: String?
    let advanceCreatedDate, advanceModifyDate: String?

    enum CodingKeys: String, CodingKey {
        case advanceID = "advance_id"
        case taFormID = "ta_form_id"
        case loginID = "login_id"
        case advanceDate = "advance_date"
        case advanceType = "advance_type"
        case advanceAmount = "advance_amount"
        case advanceCreateBy = "advance_create_by"
        case advanceModifyBy = "advance_modify_by"
        case advanceCreatedDate = "advance_created_date"
        case advanceModifyDate = "advance_modify_date"
    }
}

// MARK: - ExpensesList
struct ExpensesList: Codable {
    let expenseID, taFormID, loginID, expenseCash: String?
    let expenseCard, expenseRemark, expenseType, expenseTypeID: String?
    let expenseCreatedDate, expenseModifyDate, expenseCreateBy, expenseModifyBy: String?

    enum CodingKeys: String, CodingKey {
        case expenseID = "expense_id"
        case taFormID = "ta_form_id"
        case loginID = "login_id"
        case expenseCash = "expense_cash"
        case expenseCard = "expense_card"
        case expenseRemark = "expense_remark"
        case expenseType = "expense_type"
        case expenseTypeID = "expense_type_id"
        case expenseCreatedDate = "expense_created_date"
        case expenseModifyDate = "expense_modify_date"
        case expenseCreateBy = "expense_create_by"
        case expenseModifyBy = "expense_modify_by"
    }
}

// MARK: - JobTaformList
struct JobTaformList: Codable {
    let jobID, jobNumber, jobDate, jobClientName: String?
    let jobFrom, jobTo, jobChargeType, truckNumberid: String?
    let truckNumber, jobCommision, jobRemark, overnightNotes: String?
    let overnightFromdate, overnightTodate, overnightType, overnightAmount: String?
    let overnightDays, overnightTotalAmount, taFormID, loginID: String?
    let jobCreatedDate, jobModifyDate, jobCreatedBy, jobModifyBy: String?

    enum CodingKeys: String, CodingKey {
        case jobID = "job_id"
        case jobNumber = "job_number"
        case jobDate = "job_date"
        case jobClientName = "job_client_name"
        case jobFrom = "job_from"
        case jobTo = "job_to"
        case jobChargeType = "job_charge_type"
        case truckNumberid = "truck_numberid"
        case truckNumber = "truck_number"
        case jobCommision = "job_commision"
        case jobRemark = "job_remark"
        case overnightNotes = "overnight_notes"
        case overnightFromdate = "overnight_fromdate"
        case overnightTodate = "overnight_todate"
        case overnightType = "overnight_type"
        case overnightAmount = "overnight_amount"
        case overnightDays = "overnight_days"
        case overnightTotalAmount = "overnight_total_amount"
        case taFormID = "ta_form_id"
        case loginID = "login_id"
        case jobCreatedDate = "job_created_date"
        case jobModifyDate = "job_modify_date"
        case jobCreatedBy = "job_created_by"
        case jobModifyBy = "job_modify_by"
    }
}

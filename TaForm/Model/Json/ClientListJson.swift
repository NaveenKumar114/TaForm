//
//  ClientListJson.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-03.
//

import Foundation

// MARK: - ClientListJSON
struct ClientListJSON: Codable {
    let code: Int?
    let response: String?
    let clientsList: [ClientsList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case clientsList = "clients_list"
    }
}

// MARK: - ClientsList
struct ClientsList: Codable {
    let clientID, clientName, clientDetails, createdBy: String?
    let createdID, createdDate, updatedBy, updatedDate: String?

    enum CodingKeys: String, CodingKey {
        case clientID = "client_id"
        case clientName = "client_name"
        case clientDetails = "client_details"
        case createdBy = "created_by"
        case createdID = "created_id"
        case createdDate = "created_date"
        case updatedBy = "updated_by"
        case updatedDate = "updated_date"
    }
}

//
//  ConstantsUsedInApp.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-01.
//

import Foundation
import UIKit
struct constantsUsedInApp {
   static var accentColor : UIColor = #colorLiteral(red: 0.1656859219, green: 0.4561553001, blue: 0.6604655981, alpha: 1)
    static var baseUrl = "https://glowfreight.com.my/taform_demo/taformapi/"
}

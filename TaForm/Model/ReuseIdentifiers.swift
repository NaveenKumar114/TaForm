//
//  ReuseIdentifiers.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-06.
//

import Foundation
struct reuseCellIdentifier
{
    static var advanceCell = "AdvanceTableCell"
    static var expenseCell = "ExpenseTableCell"
    static var myBookkings = "MyBookingsCell"
    static var multiJOb = "MultiJobCell"
    static var driverList = "DriverListCell"
    static var dashboard = "DashboardCell"
    static var clientList = "ClientListCell"
    static var myJob = "MyJobCell"
}
struct reuseNibIdentifier
{
    static var advanceCell = "AdvanceTableCell"
    static var expenseCell = "ExpenseTableCell"
    static var myBookkings = "MyBookingsCell"
    static var multiJOb = "MultiJobCell"
    static var driverList = "DriverListCell"
    static var dashboard = "DashboardCell"
    static var clientList = "ClientListCell"
    static var myJob = "MyJobCell"

}

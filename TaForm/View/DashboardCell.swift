//
//  DashboardCell.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-13.
//

import UIKit

class DashboardCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

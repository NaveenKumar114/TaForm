//
//  ElevateAndCorner.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-01.
//

import Foundation
import UIKit
import Foundation
@IBDesignable class ElevateAndCorner: UIView {
    let darkShadow = CALayer()
    let lightShadow = CALayer()
    func setshadow() {
    darkShadow.frame = self.bounds
    darkShadow.cornerRadius = 15
    darkShadow.backgroundColor = UIColor.white.cgColor
    darkShadow.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
    darkShadow.shadowOffset = CGSize(width: 10, height: 10)
    darkShadow.shadowOpacity = 1
    darkShadow.shadowRadius = 15
    self.layer.insertSublayer(darkShadow, at: 0)
    lightShadow.frame = self.bounds
    lightShadow.cornerRadius = 15
        lightShadow.backgroundColor = UIColor.white.cgColor
    lightShadow.shadowColor = UIColor.white.withAlphaComponent(0.9).cgColor
    lightShadow.shadowOffset = CGSize(width: -5, height: -5)
    lightShadow.shadowOpacity = 1
    lightShadow.shadowRadius = 15
    self.layer.insertSublayer(lightShadow, at: 0)
    }
    
 
    override func layoutSubviews() {
        super.layoutSubviews()
        //print("Layout subviews!")
        //setshadow()
        self.elevate(elevation: 5)
        //self.elevateAndRounder(elevation: 5, color: UIColor.black.cgColor)
    }

}

extension UIView
{
    func elevate(elevation: Double) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: elevation)
        self.layer.shadowRadius = CGFloat(elevation)
        self.layer.shadowOpacity = 0.1
        self.layer.cornerRadius = 10
       // self.clipsToBounds = true
    }
}

extension UIView
{
    func elevateAndRounder(elevation: Double , color: CGColor)
    {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: elevation)
        self.layer.shadowRadius = CGFloat(elevation)
        self.layer.shadowOpacity = 0.24
        self.layer.borderWidth = 1
        self.layer.borderColor = color
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}


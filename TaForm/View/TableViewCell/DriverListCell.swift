//
//  DriverListCell.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-13.
//

import UIKit

class DriverListCell: UITableViewCell {

    @IBOutlet weak var lic: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var mobile: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

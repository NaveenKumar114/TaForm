//
//  MyBookingsCell.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-09.
//

import UIKit

class MyBookingsCell: UITableViewCell {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var taNumber: UILabel!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var payToDriver: UILabel!
    @IBOutlet weak var manage: UIImageView!
    @IBOutlet weak var account: UIImageView!
    @IBOutlet weak var operation: UIImageView!
    @IBOutlet weak var advance: UILabel!
    @IBOutlet weak var comission: UILabel!
    @IBOutlet weak var createBy: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

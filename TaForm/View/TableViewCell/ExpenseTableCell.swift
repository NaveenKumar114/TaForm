//
//  ExpenseTableCell.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-06.
//

import UIKit

class ExpenseTableCell: UITableViewCell {

    @IBOutlet weak var cashLabel: UILabel!
    @IBOutlet weak var notesLabel: UILabel!
    @IBOutlet weak var expenseTypeLabel: UILabel!
    @IBOutlet weak var cardLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

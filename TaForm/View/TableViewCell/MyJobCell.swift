//
//  MyJobCell.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-15.
//

import UIKit

class MyJobCell: UITableViewCell {
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var taNumber: UILabel!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var tableviewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var multiJobTableView: UITableView!
    @IBOutlet weak var payToDriver: UILabel!
    @IBOutlet weak var manage: UIImageView!
    @IBOutlet weak var account: UIImageView!
    @IBOutlet weak var operation: UIImageView!
    @IBOutlet weak var advance: UILabel!
    @IBOutlet weak var comission: UILabel!
    @IBOutlet weak var createBy: UILabel!
    var jobDetails : [JobTaformList]?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        multiJobTableView.delegate = self
        multiJobTableView.dataSource = self
        multiJobTableView.register(UINib(nibName: reuseNibIdentifier.multiJOb, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.multiJOb)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension MyJobCell: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = multiJobTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.multiJOb) as! MultiJobCell
        let x = jobDetails![indexPath.row]
        cell.date.text = x.jobDate
        cell.to.text = x.jobTo?.uppercased()
        cell.from.text = x.jobFrom?.uppercased()
        cell.type.text = x.jobChargeType
        cell.number.text = x.truckNumber
        cell.jobNo.text = x.jobNumber
        let y = Double(x.jobCommision ?? "0.0")
        let s = String(format: "%.2f", y ?? 0.0)

        cell.comission.text = "\(s) MYR"
       // print(x)
        return cell

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let c = jobDetails?.count ?? 0
        let h = (c * 100)
       // tableviewHeight.constant = CGFloat(h)
        print("row")
        print(h)
        return 100
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let c = jobDetails?.count ?? 0
        let h = (c * 100)
        tableviewHeight.constant = CGFloat(h)
        print("row")
        print(h)
        return jobDetails?.count ?? 0
    }

    
    
}


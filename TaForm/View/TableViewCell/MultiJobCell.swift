//
//  MultiJobCell.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-10.
//

import UIKit

class MultiJobCell: UITableViewCell {

    @IBOutlet weak var jobNo: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var comission: UILabel!
    @IBOutlet weak var from: UILabel!
    @IBOutlet weak var to: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

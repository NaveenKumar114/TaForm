//
//  ClientListCell.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-05-04.
//

import UIKit

class ClientListCell: UITableViewCell {

    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var clientAddress: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

//
//  DashboardStyle.swift
//  TaForm
//
//  Created by Naveen Natrajan on 2021-04-13.
//

import UIKit
import Foundation
@IBDesignable class DashboardStyle: UIView {
    let darkShadow = CALayer()
    let lightShadow = CALayer()
    func setshadow() {
    darkShadow.frame = self.bounds
    darkShadow.cornerRadius = 15
    darkShadow.backgroundColor = UIColor.white.cgColor
    darkShadow.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
    darkShadow.shadowOffset = CGSize(width: 10, height: 10)
    darkShadow.shadowOpacity = 1
    darkShadow.shadowRadius = 15
    self.layer.insertSublayer(darkShadow, at: 0)
    lightShadow.frame = self.bounds
    lightShadow.cornerRadius = 15
        lightShadow.backgroundColor = UIColor.white.cgColor
    lightShadow.shadowColor = UIColor.white.withAlphaComponent(0.9).cgColor
    lightShadow.shadowOffset = CGSize(width: -5, height: -5)
    lightShadow.shadowOpacity = 1
    lightShadow.shadowRadius = 15
    self.layer.insertSublayer(lightShadow, at: 0)
    }
    
 
    override func layoutSubviews() {
        super.layoutSubviews()
        //print("Layout subviews!")
        //setshadow()
        self.elevate(elevation: 5)
        //self.elevateAndRounder(elevation: 5, color: UIColor.black.cgColor)
    }

}

